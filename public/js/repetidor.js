$(document).ready(function () {
  
  $('.repeater').repeater({
    // (Optional)
    // start with an empty list of repeaters. Set your first (and only)
    // "data-repeater-item" with style="display:none;" and pass the
    // following configuration flag
    initEmpty: true,
    
    // (Optional)
    // "show" is called just after an item is added.  The item is hidden
    // at this point.  If a show callback is not given the item will
    // have $(this).show() called on it.
    show: function () {
      $(this).slideDown();
    },
    
  })
/*$('#fecha_nacimiento').on('changeDate', function(){
    var edad= moment(). diff($(this).val(), 'years')
    $('#edad').val(edad);
})*/ 
  function validate_fecha(fecha)
{
    var patron=new RegExp("^(19|20)+([0-9]{2})([-])([0-9]{1,2})([-])([0-9]{1,2})$");

    if(fecha.search(patron)==0)

    {
        var values=fecha.split("-");
        if(isValidDate(values[2],values[1],values[0]))
        {
            return true;
        }
    }
    return false;
}

function isValidDate(day,month,year)

{

    var dteDate;

    month=month-1;

    dteDate=new Date(year,month,day);

    return ((day==dteDate.getDate()) && (month==dteDate.getMonth()) && (year==dteDate.getFullYear()));
}

  function calcularEdad()

{

    var fecha=document.getElementById("fecha_nacimiento").value;

    if(validate_fecha(fecha)==true)

    {

      // Si la fecha es correcta, calculamos la edad

        var values=fecha.split("-");

        var dia = values[2];

        var mes = values[1];

        var ano = values[0];

        // cogemos los valores actuales

        var fecha_hoy = new Date();

        var ahora_ano = fecha_hoy.getYear();

        var ahora_mes = fecha_hoy.getMonth()+1;

        var ahora_dia = fecha_hoy.getDate();

        // realizamos el calculo

        var edad = (ahora_ano + 1900) - ano;

        if ( ahora_mes < mes )
        {
            edad--;
        }
        if ((mes == ahora_mes) && (ahora_dia < dia))
        {
            edad--;
        }
        if (edad > 1900)
        {
            edad -= 1900;
        }
        // calculamos los meses

        var meses=0;

        if(ahora_mes>mes)

            meses=ahora_mes-mes;

        if(ahora_mes<mes)

            meses=12-(mes-ahora_mes);

        if(ahora_mes==mes && dia>ahora_dia)

            meses=11;

        // calculamos los dias
        var dias=0;

        if(ahora_dia>dia)

            dias=ahora_dia-dia;

        if(ahora_dia<dia)

        {

            ultimoDiaMes=new Date(ahora_ano, ahora_mes, 0);

            dias=ultimoDiaMes.getDate()-(dia-ahora_dia);

        }
$('#edad').val(edad);

        $("#edad").prop("readonly", true);

        $.post(
            'validar',
            {'edad': edad, "_token": document.head.querySelector('meta[name="csrf-token"]').content},
            'json'
        ).done(function(data) {
            if(!!data.categoria)
            {
                $('input[name="categoria"]').val(data.categoria.id);
                $('#categoria').val(data.categoria.nombre_categoria);
                 $.post(
                        'validar_escenario',
                        {'categoria': data.categoria.id, "_token": document.head.querySelector('meta[name="csrf-token"]').content},
                        'json'
                    ).done(function(data) {
                    
                        $('select[name=escenario]').html(data);
                    });
                $('#escenario').val(data.id);
            } else {
                alert("No cumple con las edades requeridas");
            }
        });

    }else{
        document.getElementById("result").innerHTML="La fecha "+fecha+" es incorrecta";
    }
}
 $('#escenario').change(function(){
 if ($(this).val()) {
  $.post('validar_horario',{'escenarios': $(this).val(), "_token": document.head.querySelector('meta[name="csrf-token"]').content},
                        'json')
  .done(function(data) {
                    
                        $('select[name=horario]').html(data);
                    });

 }
 });


      $('#fecha_nacimiento').datepicker({
          minDate: new Date(2001,1-1,12), maxDate: '2013-12-12',
      dateFormat: 'yy-mm-dd',
      yearRange: "-17:-4",
      changeMonth: true,
      changeYear: true,
      //calculo de edad sin hacer click
      }).on('change', function(e){
        calcularEdad();
      });

 $('select[data-readonly]').on('change', function(e){

    var input = $(this).data('readonly');
    var readonly_value = $(this).data('readonly-value');
    if(readonly_value != $(this).val())
    {

      $('*[name="'+input+'"]').attr('readonly', 'readonly');

    } else {

      $('*[name="'+input+'"]').removeAttr('readonly');  

    }

  });

$('#edad').click(function(){



  calcularEdad();

});
  
  //33


  $('#poblacion').on('change', function () {
    
    if ($(this).val() == 2) { // Si el escenario es OTROS
      $("#discapacidad").prop('disabled', false);
    } else {
      $("#discapacidad").prop('disabled', 'disabled');
      $("#discapacidad").val('') 
    }
  })
  

  // validar conteo de cupos 
 $('#horario').on('change', function(e){
  $.post(
            'contador',
            {'horario': $(this).val(), 'categoria':$('#id_categoria').val(), "_token": document.head.querySelector('meta[name="csrf-token"]').content},
            'json'
        ).done(function(data) {
          $('#contador').text(data.contador)
          $('#inscritos').text(data.inscritos)
          $('#restantes').text(data.restantes)
        });
   

  });


   // cargar upz
 $('#localidad').on('change', function(e){
  $('#upz').prop('disabled', 'disabled');
  $('#upz').html("")
  $('#barrio').prop('disabled', 'disabled');
  $('#barrio').html("")
  $.post(
            'validar_localidad',
            {'localidad': $(this).val(),  "_token": document.head.querySelector('meta[name="csrf-token"]').content},
            'json'
        ).done(function(data) {
          $('#upz').html(data)
          $("#upz").prop('disabled', false);
        });
   

  });

    // cargar barrio
 $('#upz').on('change', function(e){
   $('#barrio').prop('disabled', 'disabled');
  $('#barrio').html("")
  $.post(

            'validar_upz',
            {'upz': $(this).val(),  "_token": document.head.querySelector('meta[name="csrf-token"]').content},
            'json'
        ).done(function(data) {
          $('#barrio').html(data)
          $("#barrio").prop('disabled', false);
          
        });
   

  });

$(function () {
  $('[data-toggle="popover"]').popover()
})

});