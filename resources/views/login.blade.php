@extends('master')                              

@section('content') 

<!-- Background -->

<section class="section section-shaped section-lg ">
    <div class="shape bg-gradient-info shape-style-1 shape-default">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
    <div class="container pt-lg pb-300">
        <div class="row text-center justify-content-center">
            <div class="col-lg-10">
                <h2 class="display-3 text-white">Información personal</h2>
            </div>
        </div>
    </div>
    <!-- SVG separator -->
    <div class="separator separator-bottom separator-skew zindex-100">
        <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
            <polygon class="fill-white" points="2560 0 2560 100 0 100"></polygon>
        </svg>
    </div>
</section>
<!-- Form -->
<form method="POST" action="logear" id="form_gen" enctype="multipart/form-data">
<section class="section section-lg pt-lg-0 section-contact-us" id="formulario">

<input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="container">
        <div class="row justify-content-center mt--300">
            <div class="col-lg-12">
                <div class="card bg-gradient-secondary shadow">
                    <div class="card-body p-lg-5">
                        <p class="mt-0">Todos los campos con * son obligatorios.</p>
                        <div class="form-group mt-5">
                            <div class="input-group input-group-alternative">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="ni ni-badge"></i></span>
                                </div>
                                <input required="" class="form-control" placeholder="* Ususario" type="text" id="usuario" name="usuario">
                            </div>
                        </div>
                       
                       <div class="form-group mt-5">
                            <div class="input-group input-group-alternative">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="ni ni-badge"></i></span>
                                </div>
                                <input required="" class="form-control" placeholder="* Contraseña" type="password" id="pass" name="pass">
                            </div>
                        </div>

                        <div>
                            <button type="submit" class="btn btn-info btn-round btn-block btn-lg" value="enviar">Enviar</button>
                            
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>




@stop
