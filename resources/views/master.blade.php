<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="{{ asset('public/favicon.ico') }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'IDRD - Complejo Acuático') }}</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

    <!--    Bootstrap, Fonts and icons     -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link href="{{ asset('public/css/app.css?v=2.1.0') }}" rel="stylesheet">
    <link href="{{ asset('public/js/bootstrap-datepicker/css/bootstrap-datepicker.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">


   
    <style>
        .carousel-caption {
            background-color: rgba(0, 0, 0, 0.5);
        }

     #bogota-text-group {
    animation: bogota-text 5000ms linear 1 normal forwards
  }
  
  @keyframes bogota-text {
    0% {
      opacity: 0
    }
    56.666667% {
      opacity: 0
    }
    90% {
      opacity: 1
    }
    100% {
      opacity: 1
    }
  }
  
  #big-star-shape {
    animation: big-star 5000ms linear 1 normal forwards
  }
  
  @keyframes big-star {
    0% {
      transform: translate(-1730.549201px, -361.085000px)
    }
    33.333333% {
      transform: translate(0px, 0px)
    }
    100% {
      transform: translate(0px, 0px)
    }
  }
  
  #small-star-left-shape {
    animation: small-star-left 5000ms linear 1 normal forwards
  }
  
  @keyframes small-star-left {
    0% {
      transform: translate(-1791.346249px, 924.343848px)
    }
    43.333333% {
      transform: translate(0px, 0px)
    }
    100% {
      transform: translate(0px, 0px)
    }
  }
  
  #small-star-right-shape {
    animation: small-star-right 5000ms linear 1 normal forwards
  }
  
  @keyframes small-star-right {
    0% {
      transform: translate(706.262323px, -146.590000px)
    }
    46.666667% {
      transform: translate(0px, 0px)
    }
    100% {
      transform: translate(0px, 0px)
    }
  }
  
  #idrd-group-text {
    animation: idrd-text 5000ms linear 1 normal forwards
  }
  
  @keyframes idrd-text {
    0% {
      opacity: 0;
      transform: translate(14.190544px, 309.353848px)
    }
    86.666667% {
      opacity: 0;
      transform: translate(14.190544px, 309.353848px)
    }
    100% {
      opacity: 1;
      transform: translate(0px, 0px)
    }
  }
    </style>

</head>
<body class="contact-page sidebar-collapse">

<!--    navbar come here          -->
@include('sections._nav-bar')
<!-- end navbar  -->

<div class="wrapper" id="app">
    @yield('content')
</div>

<!-- Footer come here -->
@include('sections._footer')
<!--   end footer -->


</body>



<!--  Plugins -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key="></script>
<script src="{{ asset('public/js/manifest.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/js/vendor.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/js/moment.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/js/app.js') }}" type="text/javascript"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!--para  muñequitos-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bodymovin/5.7.3/lottie.min.js" integrity="sha512-35O/v2b9y+gtxy3HK+G3Ah60g1hGfrxv67nL6CJ/T56easDKE2TAukzxW+/WOLqyGE7cBg0FR2KhiTJYs+FKrw==" crossorigin="anonymous"></script>



<script src="{{ asset('public/js/jquery.repeater.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/js/repetidor.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/js/maxLength.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/js/jquery-ui.js') }}" type="text/javascript"></script>

@yield('scripts')


</html>