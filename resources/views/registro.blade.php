

@extends('master')
@section('content')

<div class="wrapper">
   <div class="page-header page-header-xs" data-parallax="true" style="background-image: url('{{ asset('public/images/pool.jpg') }}');">
      <div class="filter"></div>
      <div class="content-center">
         <div class="container">
            <div class="title-brand">
               <h1>
                  IDRD 2020
               </h1>
               
            </div>
            <h5 class="text-center" style="color:#ffffff">
               ESCUELA DEPORTIVA DE NATACIÓN COMPLEJO ACUÁTICO SIMÓN BOLIVAR
            </h5>
         </div>
      </div>
   </div>
   <div class="section profile-content">
      <div class="container">
         <div class="card-body">
            <div class="card-description">

        
   <form method="POST" action="insertar" id="form_gen" enctype="multipart/form-data">

   {{csrf_field()}}
   
      <div class="panel-body">
         <p align="center" style="font-size: 22px; color: #5D4593; font-weight: bolder; font-weight: bolder;"><strong>FORMULARIO DE PRE-INSCRIPCIÓN NIÑOS DE 4 A 7 AÑOS</strong></p>
         
         <p align="center"><font size="3" style="font-weight: bolder;">Pre-Inscripciones y formalizacines entre el día 07 de noviembre hasta el 12 de noviembre o agotar cupos disponibles </font></p><br>
         <p style="line-height: 27px; font-size: 12pt; font-weight: bolder;" align="justify">Este Primer momento o tramite  se realiza a través de este medio electrónico, para el segundo momento o Formalización de la Inscripción debe dirigirse al escenario donde va a tomar el curso, entre el día 07 al 12 de noviembre de 2017 según programación en un horario de 8:00 a.m a 12:00 m. y de 2:00 a 5:00 p.m., con el objeto de entregar los documentos solicitados por el IDRD y de esta manera Formalizar su Inscripción. <strong>si NO realiza la entrega de documentos</strong> se anulara la Pre-Inscripción y perderá el cupo.</p><br>
         <p style="line-height: 27px; font-size: 12pt; font-weight: bolder;" align="justify"><strong>IMPORTANTE:</strong> Tenga en cuenta que solo debe realizar la formalización de la inscripción del curso cuando la Pre-inscripción sea aceptada <strong>(el sistema automáticamente le informa de la Aceptación de la Pre-inscripción al finalizar este proceso)</strong> y  solo debe inscribir al niño(a) una sola y única vez, durante este proceso, con el objeto de evitar duplicidad en la información y de esta forma excluir de un cupo a otro usuario interesado. </p>
         
         <br>
           


<!--nuevo nuevo formulario información niño-->
               <div class="card">
                 <h5 class="card-header" style="color: #5D4593; font-weight: bolder"><strong>INFORMACIÓN DEL NIÑO</strong></h5>
                 <div class="card-body">
                      <div class="row">
                           <div class="col-md-4">
                              <label class="freebirdFormviewerViewItemsItemItemTitle" for="formGroupExampleInput" style="font-weight: bolder;" style="font-weight: bolder;">Nombres Completos </label>
                              <input required type="text" class="form-control" id="nombre_nino" name="nombre_nino" onkeyup="javascript:this.value=this.value.toUpperCase();"  autocomplete="off">
                           </div>
                           <div class="col-md-4">
                              <label class="freebirdFormviewerViewItemsItemItemTitle" for="formGroupExampleInput2" style="font-weight: bolder;">Apellidos Completos</label>
                              <input required type="text" class="form-control" id="apellido_nino" name="apellido_nino" onkeyup="javascript:this.value=this.value.toUpperCase();"  autocomplete="off"><br>
                           </div>
                           <div class="col-md-4">
                              <label class="freebirdFormviewerViewItemsItemItemTitle" for="formGroupExampleInput" style="font-weight: bolder;">Documento de Identificación </label>
                              <input required type="text" class="form-control" id="cedula" name="cedula"  autocomplete="off">
                           </div>
                          
                           <div class="col-md-4">
                              <label class="freebirdFormviewerViewItemsItemItemTitle" for="formGroupExampleInput" style="font-weight: bolder;">Fecha de nacimiento </label>
                              <input type='text' class="form-control"   name="fecha_nacimiento" id="fecha_nacimiento" autocomplete="off"  readonly="" />
                           </div>
                           <div class="col-md-4">
                              <label class="freebirdFormviewerViewItemsItemItemTitle" for="formGroupExampleInput2" style="font-weight: bolder;" style="font-weight: bolder;">Edad cumplida</label>
                              <input required type="text" class="form-control" id="edad" name="edad" readonly="" autocomplete="off"><br>
                           </div>
                            <div class="col-md-4">
                              <label class="freebirdFormviewerViewItemsItemItemTitle" for="formGroupExampleInput2" style="font-weight: bolder;" style="font-weight: bolder;">Sexo</label>
                              <select name="sexo" id="sexo" class="form-control"  autocomplete="off">
                                 <option value="">Seleccione</option>
                                 <option value="Mujer">Mujer</option>
                                 <option value="Hombre">Hombre</option>
                              </select>
                              <br>
                           </div>
                            <div class="col-md-4">
                              <label class="freebirdFormviewerViewItemsItemItemTitle" for="formGroupExampleInput2" style="font-weight: bolder;">Identidad de género</label>
                              <select name="genero" id="genero" class="form-control"  autocomplete="off">
                                 <option value="">Seleccione</option>
                                 <option value="Femenino">Femenino</option>
                                 <option value="Masculino">Masculino</option>
                                 <option value="Transgénero">Transgénero</option>
                              </select>
                              <br>
                           </div>
                           <div class="col-md-4">
                              <label class="freebirdFormviewerViewItemsItemItemTitle" for="formGroupExampleInput2" style="font-weight: bolder;">Grupo Étnico</label>
                              <select name="etnia" id="etnia" class="form-control"  autocomplete="off">
                                 <option value="">Seleccione</option>
                                 <option value="Afro">Afro</option>
                                 <option value="Raizal">Raizal</option>
                                 <option value="Indigena">Indigena</option>
                                 <option value="Gitano">Gitano</option>
                                 <option value="Mestizo">Mestizo</option>

                              </select>
                              <br>
                           </div>
                           <div class="col-md-4">
                              <label class="freebirdFormviewerViewItemsItemItemTitle" for="formGroupExampleInput2" style="font-weight: bolder;">Grupo social  y poblacional</label>
                              <select  required name="poblacion" id="poblacion" class="form-control" >
                                <option value="">Seleccione</option>
                                @foreach ($poblaciones as $poblacion)
                                <option value="{{ $poblacion->id}}">{{$poblacion->nombre}}</option>
                                @endforeach
                             </select>
                              <br>
                           </div>


                             <div class="col-md-4">
                              <label class="freebirdFormviewerViewItemsItemItemTitle" for="formGroupExampleInput2" style="font-weight: bolder;">Discapacidad</label>
                              <select name="discapacidad" id="discapacidad" class="form-control" disabled= "disabled">
                                 <option value="">Seleccione</option>
                                 <option value="Física">Física</option>
                                 <option value="Visual">Visual</option>
                                 <option value="Auditiva ">Auditiva </option>
                                 <option value="Cognitiva">Cognitiva</option>
                                
                              </select>
                              <br>
                           </div>
                           <div class="col-md-4">
                              <label class="freebirdFormviewerViewItemsItemItemTitle" for="formGroupExampleInput" style="font-weight: bolder;">Dirección de residencia </label>
                              <input required type="text" class="form-control" id="direccion_nino" name="direccion_nino"  autocomplete="off">
                           </div>
                           <div class="col-md-4">
                              <label class="freebirdFormviewerViewItemsItemItemTitle" for="formGroupExampleInput" style="font-weight: bolder;">EPS </label>
                              <input required type="text" class="form-control" id="eps" name="eps" onkeyup="javascript:this.value=this.value.toUpperCase();" >
                              <br>
                           </div>
                           <div class="col-md-4">
                              <label class="freebirdFormviewerViewItemsItemItemTitle" for="formGroupExampleInput" style="font-weight: bolder;">Localidad </label>
                              <select  required name="localidad" id="localidad" class="form-control" >
                                <option value="">Seleccione</option>
                                @foreach ($localidades as $localidad)
                                <option value="{{ $localidad->Id_Localidad }}">{{ $localidad->Localidad}}</option>
                                @endforeach
                             </select>
                           </div>
                            <div class="col-md-4">
                              <label class="freebirdFormviewerViewItemsItemItemTitle" for="formGroupExampleInput" style="font-weight: bolder;">UPZ</label>
                              <select  required name="upz" id="upz" class="form-control" disabled="">
                                <option value="">Seleccione</option>
                             </select>
                           </div>
                            <div class="col-md-4">
                              <label class="freebirdFormviewerViewItemsItemItemTitle" for="formGroupExampleInput" style="font-weight: bolder;">Barrio</label>
                              <select  required name="barrio" id="barrio" class="form-control" disabled="">
                                <option value="">Seleccione</option>
                             </select><br>
                           </div>

                            <div class="col-md-4">
                              <label class="freebirdFormviewerViewItemsItemItemTitle" for="formGroupExampleInput2" style="font-weight: bolder;">Estrato</label>
                              <select name="estrato" id="estrato" class="form-control">
                                 <option value="">Seleccione</option>
                                 <option value="1">1</option>
                                 <option value="2">2</option>
                                 <option value="3">3 </option>
                                 <option value="4">4</option>
                                 <option value="5">5</option>
                                 <option value="6">6</option>
                              </select>
                           </div>

                           <div class="col-md-4">
                              <label class="freebirdFormviewerViewItemsItemItemTitle" for="formGroupExampleInput2" style="font-weight: bolder;">Institución Educativa</label>
                              <input required type="text" class="form-control" id="institucion" name="institucion" onkeyup="javascript:this.value=this.value.toUpperCase();"  autocomplete="off" ><br>
                           </div>
                           <div class="col-md-4">
                              <label class="freebirdFormviewerViewItemsItemItemTitle" for="formGroupExampleInput" style="font-weight: bolder;">Sector del colegio </label>
                              <select name="sector_colegio" id="sector_colegio" class="form-control"  autocomplete="off">
                                 <option value="">Seleccione</option>
                                 <option value="Público">Público</option>
                                 <option value="Privado">Privado</option>
                              </select>
                           </div>
                     </div>
                 </div>
               </div>
<!--fin nuevo nuevo formulario niño-->   


<!--nuevo información del curso-->
               <div class="card">
                 <h5 class="card-header" style="color: #5D4593; font-weight: bolder"><strong>INFORMACIÓN DEL CURSO</strong></h5>
                 <div class="card-body">
                     <div class="row">
                           <div class="col-md-6">
                             <label class="freebirdFormviewerViewItemsItemItemTitle" for="formGroupExampleInput" style="font-weight: bolder;">Categoría </label>
                                 <input type="text" id="categoria"  readonly="" value="" class="form-control"  autocomplete="off">
                                 <input type="hidden" id="id_categoria" name="categoria">
                           </div>
                           
                           <div class="col-md-6">
                           <label class="freebirdFormviewerViewItemsItemItemTitle" for="formGroupExampleInput" style="font-weight: bolder;">Escenario o CEFE </label>
                           <select  required name="escenario" id="escenario" class="form-control"  autocomplete="off">
                             <option value="">Seleccione</option>
                       
                            </select><br><br>
                           </div>
                           
                         <div class="col-md-6">
                             <label class="freebirdFormviewerViewItemsItemItemTitle" for="formGroupExampleInput" style="font-weight: bolder;"><font color="#5D4593"><u>HORARIO DEL CURSO</u> </font></label>
                             <select  required name="horario" id="horario" class="form-control"  autocomplete="off">
                                <option value="">Seleccione</option>
                               </select><br>
                        
                          </div>
                           <div class="col-md-3">&nbsp;</div>  
                           <div class="col-md-3">&nbsp;</div>  
                           <!--total de cupos en el horario-->
                           <div class="col-md-4"><label style="color:#5D4593"> <strong>Total cupos habilitados en este horario</strong></label><p id="contador" style="font-size: 18pt;color: #0B610B; font-weight: bolder;">0</p></div>  

                           <!--total de nadadores inscritos -->
                          <div class="col-md-4"><label style="color:#5D4593"> <strong>Total inscritos en este horario</strong> </label><p id="inscritos" style="font-size: 18pt;color: black; font-weight: bold;">0</p></div> 

                          <!--total cupos restantes -->
                           <div class="col-md-4"><label style="color:#5D4593"> <strong>Total cupos restantes en este horario</strong> </label><p id="restantes" style="font-size: 18pt;color: red;font-weight: bold;">0</p></div> 

                              </div> 
                              <div class="col-md-4">&nbsp;</div>        
                           <div class="col-md-12"><label style="font-weight: bolder;" > si selecciona <font color="#d81631" size="3">SI</font> se realizará evaluación de Habilidades en el agua, y se ubicara en el mismo horario u otro disponible según resultado y cupos para el nivel evaluado</label></div>    
                              </div> 
                    </div>
               </div>
<!--fin nuevo información del curso-->               

<!--nuevo nuevo formulario información acudiente-->
               <div class="card">
                 <h5 class="card-header" style="color: #5D4593; font-weight: bolder"><strong>INFORMACIÓN DEL ACUDIENTE O REPRESENTANTE LEGAL</strong></h5>
                 <div class="card-body">
                     <div class="row">
                           <div class="col-md-4">
                              <label class="freebirdFormviewerViewItemsItemItemTitle" for="formGroupExampleInput" style="font-weight: bolder;">Nombre Completo </label>
                              <input required type="text" class="form-control" id="nombre_acudiente" name="nombre_acudiente" onkeyup="javascript:this.value=this.value.toUpperCase();" autocomplete="off">
                           </div>
                           
                           <div class="col-md-4">
                              <label class="freebirdFormviewerViewItemsItemItemTitle" for="formGroupExampleInput2" style="font-weight: bolder;">Documento de Identidad </label>
                              <input title="Se necesita una cedula" required type="number" class="form-control" id="cedula_acudiente" name="cedula_acudiente" autocomplete="off"><br>
                           </div>
                           
                           <div class="col-md-4">
                              <label class="freebirdFormviewerViewItemsItemItemTitle" for="formGroupExampleInput" style="font-weight: bolder;">Ocupación </label>
                              <input required type="text" class="form-control" id="ocupacion" name="ocupacion" onkeyup="javascript:this.value=this.value.toUpperCase();"  autocomplete="off">
                           </div>
                           <div class="col-md-4"><label class="freebirdFormviewerViewItemsItemItemTitle" for="formGroupExampleInput2" style="font-weight: bolder;">Correo Electrónico</label>
                               <input required type="mail" class="form-control" id="mail" name="mail" autocomplete="off"><br>
                           </div>
                        
                           <div class="col-md-4">
                              <label class="freebirdFormviewerViewItemsItemItemTitle" for="formGroupExampleInput" style="font-weight: bolder;">Teléfono Fijo </label>
                              <input required type="number" class="form-control" id="telefono" name="telefono"  autocomplete="off">
                           </div>
                           <div class="col-md-4"><label class="freebirdFormviewerViewItemsItemItemTitle" for="formGroupExampleInput2" style="font-weight: bolder;">Teléfono Celular</label>
                               <input required type="number" class="form-control" id="celular" name="celular" autocomplete="off" ><br>
                           </div>
                     </div> 
                 </div>
               </div>

               <input type="hidden" name="ciclo" id="ciclo" value="8-2019">
<!--fin nuevo nuevo formulario acudiente-->    

<br>

<!--consentimiento informado-->
               <div class="card">
                 <h5 class="card-header" style="color: #5D4593; font-weight: bolder"><strong>IMPORTANTE:</strong></h5>
                 <div class="card-body">
                     <div class="row">
                           <div class="col-md-12">
                              <small style="line-height: 22px; font-size: 11pt; align="justify">
                                
                                
                                * Recuerde que el horario<strong> NO PUEDE SER MODIFICADO, </strong> teniendo en cuenta la programación establecida por el IDRD y la cantidad de cupos habilitados por horario, ya que estos son limitados con el fin de ofrecer un buen servicio en el desarrollo técnico de la escuela deportiva.<br>

                                * Todos los campos son Obligatorios, Recuerde que al finalizar este proceso y ENVIAR los datos el sistema le informa automáticamente del éxito del trámite.<br>

                                * Si al diligenciar los datos requeridos en el formulario se presentan errores en los datos aquí solicitados, como; No. Documento, Fecha de Nacimiento, Edad, etc. Se puede perder el cupo asignado en la Escuela. 
                                </small><br><br>
                                </fieldset>
                                 <a class="btn btn-outline-info btn-round" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                                    </samp><strong><span class="freebirdFormviewerViewItemsItemRequiredAsterisk" aria-hidden="true">*</span>Términos de Inscripción</strong>
                                  </a>
                           </div>
                           



                           <div class="col-md-12">


                                 <div class="freebirdFormviewerViewFormContent ">
      
                                    <div class="freebirdFormviewerViewItemList" role="list">
                                       
                                       <div class="freebirdFormviewerViewItemsPagebreakDescriptionText"></div>
                                       <div role="listitem" class="freebirdFormviewerViewItemsItemItem" jsname="ibnC6b" jscontroller="hIYTQc" jsaction="JIbuQc:qzJD1c;sPvj8e:e4JwSe" data-required="true" data-other-input="qSV85" data-other-hidden="MfYA1e" data-item-id="131124881">
                                          <div class="freebirdFormviewerViewItemsItemItemheader">
                                             <div class="freebirdFormviewerViewItemsItemItemTitleContainer">


              <div class="collapse" id="collapseExample">
                <div class="card card-body">
                                                  <p style="text-align: justify; font-size: 11pt"><strong>1. </strong> Mediante el diligenciamiento de este formulario, autorizo al Instituto Distrital de Recreación y Deporte – IDRD, así como a cualquier dependencia aliada o filial del Instituto, a realizar el tratamiento de mi datos personales o los aquí consignados relacionados con mi hijo(a) o Representado menor de edad, conforme a la ley 1581 de 2012, con la finalidad de ser utilizados para realizar registro de participación en las Escuelas Deportivas y/o de Formación, Convocatorias y/o Estadísticas que realicen las Entidades o dependencias aquí nombradas.<br><br>

                                                   <strong>2. </strong> Manifiesto que he decidido inscribir a mi hijo(a) o representado de forma voluntaria en La Escuela de Formación que ofrece el Instituto Distrital de Recreación y Deporte – IDRD.<br><br>

                                                   <strong>3. </strong> He leído el Reglamento y Requisitos de la Escuela de Natación del CASB-IDRD por lo tanto acepto los términos y condiciones.<br><br>

                                                   <strong>4. </strong> Acepto y asumo todos los riesgos asociados con la participación en la Escuela de Formación para conmigo y/o mi hijo(a) o representado menor de edad incluyendo, mis propias acciones u omisiones y asumo los riesgos asociados con caídas, lastimaduras, cortadas y riesgos asociados a la ocurrencia de circunstancias de fuerza mayor tales como desastres naturales, ataques de animales, terrorismo, o no acatamiento de las normas o reglamentos en el uso del escenario.<br><br>

                                                   <strong>5. </strong> Declaro obedecer todas las instrucciones durante mi permanencia en el escenario deportivo, de igual forma las instrucciones dadas a mi hijo(a) o representado legal durante la sesión de clase o de permanencia en las zonas de cambio o transición, ya sea de forma escrita o cualquier otro medio inteligible provista por el personal del Instituto Distrital de Recreación y Deporte – IDRD o contratistas para evitar incidentes o situaciones peligrosas o prestar asistencia durante la clase. Así mismo, declaro que acepto el cuidado médico ofrecido por los paramédicos o salvavidas para mi hijo(a) o Representado legal.<br><br>

                                                   <strong>6. </strong> Exonero de toda responsabilidad al Instituto Distrital de Recreación y Deporte – IDRD, así como a sus funcionarios y contratistas, aliados, patrocinadores y/o representantes, de todo reclamo o responsabilidad contractual o extracontractual que surja de la participación de mi hijo(a) o Representado Legal en la Escuela de Formación.<br><br>

                                                   <strong>7. </strong> De igual forma declaro que mi hijo(a) o representado legal se encuentra en condiciones médicas, psicológicas y físicas aptas para asistir y participar en las clases o sesiones y me comprometo a notificar al personal del Instituto Distrital de Recreación y Deporte – IDRD sobre cualquier preexistencia o novedad en relación con su salud. Así mismo certifico que se encuentra afiliado y activo a la entidad promotora de salud – EPS relacionada en la certificación que se entrega con la Formalización de la Inscripción.</p>
                  </div>
                </div>


                                          </div><br>
                                          <div jsname="JNdkSc" role="group" aria-labelledby="i1" aria-describedby="i.desc.131124881 i.err.131124881 i.req.131124881" class="">
                                             <div class="" jsname="MPu53c" jscontroller="GJQA8b" jsaction="JIbuQc:aj0Jcf" data-value="Acepto">
                                                <div class="freebirdFormviewerViewItemsCheckboxChoice">
                                                   <label class="docssharedWizToggleLabeledContainer freebirdFormviewerViewItemsCheckboxContainer">
                                                      <div class="exportLabelWrapper">
                                                          <div class="col-md-12">
                                                             <input type="checkbox" required style="float: left; margin: 1px; width: 25px; height: 25px" name="acepto" id="acepto">&nbsp;Acepto
                                                          </div>                                        
                                                      </div>
                                                   </label>
                                                </div>
                                                <input name="entry.1642827248" jsname="ekGZBc" disabled="" type="hidden">
                                             </div>
                                          </div>
                                          <div id="i.req.131124881" class="screenreaderOnly"></div>
                                          <div jsname="XbIQze" class="freebirdFormviewerViewItemsItemErrorMessage" id="i.err.131124881" role="alert"></div>
                                       </div>
                                    </div>
                                    <div class="freebirdFormviewerViewNavigationNavControls" jscontroller="lSvzH" jsaction="rcuQ6b:npT2md;JIbuQc:V3upec(GeGHKb),HiUbje(M2UYVd),NPBnCf(OCpkoe)" data-shuffle-seed="-2327421662174229681">
                                       <div class="freebirdFormviewerViewNavigationButtonsAndProgress">
                                          <div class="freebirdFormviewerViewNavigationButtons">
                                          <center><button type="submit" class="btn btn-outline-info btn-round" value="Enviar" style="font-size: 18px">Registrar Inscripción</button></center>
                                             <!--<input class="enviar-btn btn-primary" type="submit" value="Enviar">-->
                                          </div>
                                       </div>
                                       
                                    </div>
                                    </form>
                                 </div>
                           </div>
                     </div> 
                 </div>
               </div>
<!--fin consentimiento informado -->   

        


<div id="" class="section  text-center">
   <div class="container">
      <div class="row">
         <div class="col-md-12">
           <a href="comunicados" class="btn btn-outline-info btn-round" style="font-size: 12px"><i class="fa fa-play"></i>COMUNICADOS Y NOVEDADES</a>
           <a href="inscripciones" class="btn btn-outline-info btn-round" style="font-size: 12px"><i class="fa fa-play"></i>PROCESO DE INSCRIPCIÓN</a>
           <a href="cronograma" class="btn btn-outline-info btn-round" style="font-size: 12px"><i class="fa fa-play"></i>CRONOGRAMA Y PROGRAMACIÓN</a>
           <a href="javascript:;" class="btn btn-outline-info btn-round" style="font-size: 12px"><i class="fa fa-play"></i>CONSULTAS Y DESCARGAS</a>
                     
         </div>
      </div>
   </div>
</div>
<center><img src="{{ asset('public/images/Logo-rojo.png') }}" heigth="20%"  width="20%" alt=""></center>

      </div>
   </div>
</div>
</div>
@stop

