
@extends('master')
@section('scripts')
<script type="text/javascript">
   var animation = bodymovin.loadAnimation({
  container: document.getElementById('danieles'), // Required
  path: "{{ asset('public/animaciones/nadador1.json') }}", // Required
  renderer: 'svg', // Required
  loop: true, // Optional
  autoplay: true, // Optional
  name: "nadador1", // Name for future reference. Optional.
});
 var animation2 = bodymovin.loadAnimation({
  container: document.getElementById('danieles2'), // Required
  path: "{{ asset('public/animaciones/nadador2.json') }}", // Required
  renderer: 'svg', // Required
  loop: true, // Optional
  autoplay: true, // Optional
  name: "nadador2", // Name for future reference. Optional.
})  

</script>
@stop
@section('content')
<div class="wrapper">
   <div class="page-header page-header-xs" data-parallax="true" style="background-image: url('{{ asset('public/images/pool.jpg') }}');">
      <div class="filter"></div>
      <div class="content-center">
         <div class="container">
            <div class="title-brand">
               <h1>
                  IDRD 2020
               </h1>
               
            </div>
            <h5 class="text-center" style="color:#ffffff">
               ESCUELA DEPORTIVA DE NATACIÓN COMPLEJO ACUÁTICO SIMÓN BOLIVAR
            </h5>
         </div>
      </div>
   </div>
   <div class="section profile-content">
      <div class="container">
         <div class="card-body">
            <div class="card-description">
               <div id="acordeon">
                  <div id="accordion" role="tablist" aria-multiselectable="true">
                     <div class="card no-transition">
                        <!--acordion 1-->
                        <div class="card-header card-collapse" role="tab" id="headingThree">
                           <h5 class="mb-0 panel-title">
                              <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree" style="font-weight: bolder; font-size: 18pt;color: #5D4593">
                              Consulte aquí el Procedimiento para las inscripciones a los cursos
                              <i class="nc-icon nc-minimal-down"></i>
                              </a>
                           </h5>

                        </div>


                        
                           <div class="card-body">
                              <!--acordion 2-->
                              <div class="card-header card-collapse" role="tab" id="headingOne">
                                 <h5 class="mb-0 panel-title" align="justify">
                                    La Inscripción para los nadadores está dividida en Dos (2) Momentos a saber;
                                    Preinscripción <strong>(1er Momento)</strong> y Formalización de la Inscripción <strong>(2do Momento),</strong> según su estado en la Escuela cada uno de estos momentos puede variar dependiendo si es Nadador Antiguo o desea inscribirse como Nadador Nuevo. <br><br>

                                     La preinscripción no permite cambios de horario, por disponibilidad de niveles y cupos. Cuando un alumno Nuevo ya es aceptado en la Escuela y realiza el proceso completo de Inscripción cambia su estado a “Nadador Antiguo”, para los siguientes ciclos debe realizar el procedimiento de Inscripción según este estado que se pueden consultar en la opción Nadador Antiguo. <br><br>


                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne" style="font-weight: bolder;color: #5D4593">
                                    NADADORES ANTIGUOS 
                                    <i class="nc-icon nc-minimal-down"></i>
                                    </a>
                                 </h5>
                              </div>
                              <div id="collapseOne" class="collapse" role="tabpanel" aria-labelledby="headingOne">
                                 <div class="card-body">
                                    <p align="justify" style="font-size: 16pt; font-weight:bolder;">1. Preinscripción (1er Momento)</p>
                                    Se realiza únicamente por página web <a href="https://idrd.gov.co/SIM/FORMULARIOS/Natacion_CASB_2018/" target="_black" style="font-weight: bolder;"> www.idrd.gov.co </a>link Escuela CASB-IDRD. Las fechas de Preinscripción para Nadadores(as) Antiguos se pueden observar en menú <a href="https://idrd.gov.co/SIM/FORMULARIOS/Natacion_CASB_2018/programacion" target="_black" style="font-weight: bolder;">Cronograma y Programación</a>  de esta página.<br><br>
                                    <strong><u>Nota:</u></strong> Este procedimiento para los Nadadores(as) Antiguos se realiza una sola vez en el año (1er ciclo o correspondiente de ingreso), solo si se mantiene la continuidad durante cada ciclo programado.<br><br>

                                     <p align="justify" style="font-size: 16pt; font-weight:bolder;">2. Formalización de la Inscripción (2do Momento)</p>
                                    Para Formalizar la inscripción de Nadadores(as) Antiguos se debe:
                                    <br><br>
                                    Realizar solo en las fechas establecidas en el cronograma del año <strong>(sin excepción),</strong> donde se radicarán los documentos exigidos y se entregara el soporte de pago del curso original. Ver menú Cronograma y Programación de esta página para conocer las fechas.<br><br>
                                    <strong><u>Nota 1:</u></strong> La entrega de la Documentación Exigida para los Nadadores(as) Antiguos se realiza una sola vez en el año (1er ciclo o correspondiente de ingreso) si se mantiene la continuidad durante cada ciclo programado.<br><br>
                                    <strong><u>Nota 2:</u> </strong> Para los siguientes ciclos no debe hacer preinscripción, SOLO DEBERÁ ENTREGAR EL SOPORTE DE PAGO ORIGINAL EN LA COORDINACIÓN DE LA ESCUELA DE NATACIÓN DEL IDRD-CASB EN LAS FECHAS ESTABLECIDAS (último fin de semana del ciclo que cursan), si no se hace en la fecha designada pierde el cupo asignado.<br><br>
                                    <strong><u>Nota 3:</u> </strong>Si pierde la continuidad no puede hacer el proceso como nuevo, debe acercarse a la coordinaciónde la Escuela de Natación CASB para revisar su caso específico.
                                 </div>
                              </div>
                              <!--fin acodion 2-->

                              <!--acordion 3-->
                              <div class="card-header card-collapse" role="tab" id="headingtwo">
                                 <h5 class="mb-0 panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapsetwo" aria-expanded="false" aria-controls="collapsetwo" style="font-weight: bolder;color: #5D4593">
                                    NADADORES(AS) NUEVOS 
                                    <i class="nc-icon nc-minimal-down"></i>
                                    </a>
                                 </h5>
                              </div>
                              <div id="collapsetwo" class="collapse" role="tabpanel" aria-labelledby="headingtwo">
                                 <div class="card-body">
                                     <p align="justify" style="font-size: 16pt; font-weight:bolder;">1. Preinscripción (1er Momento)</p>
                                    Se realiza únicamente por página web <a href="https://idrd.gov.co/SIM/FORMULARIOS/Natacion_CASB_2018/" target="_black"> www.idrd.gov.co </a>link Escuela CASB-IDRD, hasta completar cupos habilitados para cada ciclo. Para saber las fechas programadas para la Preinscripción en cada ciclo Ver menú <a href="https://idrd.gov.co/SIM/FORMULARIOS/Natacion_CASB_2018/programacion" target="_black">Cronograma y Programación</a>  de esta página.<br><br>

                                     <p align="justify" style="font-size: 16pt; font-weight:bolder;">2. Formalización de la Inscripción (2do Momento)</p>
                                    Para Formalizar la inscripción de Nadadores(as) Nuevos en cada ciclo se debe:
                                    <br><br>
                                    Realizar solo en las fechas establecidas en el cronograma del año (sin excepción), para cada ciclo donde se habiliten inscripciones, en ese momento se entregará el soporte de pago original del curso y la totalidad de los documentos exigidos, se entregará directamente en la Coordinación de la Escuela de Natación del IDRD-CASB en las fechas establecidas, si no se hace en la fecha designada pierde el cupo obtenido en la Preinscripción. Ver Novedades o Comunicados y el Menú Cronograma y programación de esta página para más información.
                                    <br><br>
                                    <strong><u>Nota 1:</u> </strong> los Nadadores(as) Nuevos deben realizar la consignación bancaria SOLO en el momento que tengan confirmación y aceptación de la Preinscripción realizada por la página de la Escuela, en las fechas establecidas. Recuerde que el sistema le informa al finalizar el proceso de preinscripción de la aceptación de esta. No olvide imprimir el comprobante o Formulario de Preinscripción.
                                    <br><br>
                                    <strong><u>NOTA IMPORTANTE:</u></strong> si no se cumplen las fechas programadas de preinscripción y/o Formalización se pierde el cupo asignado y deberá acercarse a la coordinación si desea ingresar a la escuela para revisar su caso.
                                 </div>
                              </div>
                              <!--fin acodion 3-->



                              <!--acordion 4-->
                              <div class="card-header card-collapse" role="tab" id="headingfour">
                                 <h5 class="mb-0 panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapsefour" aria-expanded="false" aria-controls="collapsefour" style="font-weight: bolder; color: #5D4593">
                                    DOCUMENTOS EXIGIDOS  
                                    <i class="nc-icon nc-minimal-down"></i>
                                    </a>
                                 </h5>
                              </div>
                              <div id="collapsefour" class="collapse" role="tabpanel" aria-labelledby="headingfour">
                                 <div class="card-body">
                                   
                                    <ol>
                                       <li style="line-height: 27px; font-size: 11pt" align="justify">Formulario de Preinscripción, el cual descarga en esta misma página.</li>
                                       <li style="line-height: 27px; font-size: 11pt" align="justify">Certificación médica no mayor a 2 meses de la fecha de inicio del curso, donde especifique que el nadador es apto para practicar este deporte y que no tiene enfermedades infectocontagiosas (Se permite de la EPS o Entidad Privada).</li>
                                       <li style="line-height: 27px; font-size: 11pt" align="justify">Copia del carné estudiantil o certificación del Colegio vigente o de matrícula.<strong><em>(Solo para colegios públicos).</em></strong></li>
                                       <li style="line-height: 27px; font-size: 11pt" align="justify">Certificación de afiliación a la EPS actual (no se permite copia del Carné de la EPS) no mayor a dos meses.</li>
                                       <li style="line-height: 27px; font-size: 11pt" align="justify">Fotocopia Documento de identificación (tarjeta de identidad o registro civil).</li>
                                    </ol>
                                 </div>
                              </div>
                              <!--fin acodion 4-->




                              <!--acordion 5-->
                              <div class="card-header card-collapse" role="tab" id="headingfive">
                                 <h5 class="mb-0 panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapsefive" aria-expanded="false" aria-controls="collapsefive" style="font-weight: bolder;color: #5D4593">
                                    INVERSIÓN FORMA DE PAGO DEL CURSO  
                                    <i class="nc-icon nc-minimal-down"></i>
                                    </a>
                                 </h5>
                              </div>
                              <div id="collapsefive" class="collapse" role="tabpanel" aria-labelledby="headingfive">
                                 <div class="card-body" align="justify">
                                    Recuerde que se debe colocar bien las referencias de pago del escenario y servicio o deberá nuevamente consignar el valor del curso para poder Formalizar la Inscripción, consignar el valor del curso exacto según corresponda ya que si no lo hace deberá hacer una consignación nuevamente por el valor faltante (excedente) y realizar una consignación (diligenciar un formato) por cada niño o niña inscrito(a) al curso. <br><br>
                                     <p align="justify" style="font-size: 15pt; font-weight:bolder;">- INVERSIÓN:</p>
                                    <li>Estudiantes de colegios distritales - IED es:    <strong style="font-size: 14pt"> $89.800</strong></li>
                                    <li>Estudiantes de colegios no distritales - IED es:   <strong style="font-size: 14pt"> $94.500</strong></li>
                                    <br>
                                    <p align="justify" style="font-size: 15pt; font-weight:bolder;">- FORMA DE PAGO:</p>
                              
                                    <li>Descargar aquí las nuevas disposiciones de consignación o pago del curso en sucursal bancaria.  <strong class="text-info">Consultar</strong></li>
                                
                                    <li>Descargar aquí las disposiciones de consignación o pago del curso por pagos online "Pagos PSE".    <strong class="text-info"> Consultar</strong></li>
                                    <br>
                                    <!--<li type= disc>
                                       Consignar únicamente en la <strong class="text-info">cuenta de ahorros de Davivienda No.00170002891-1</strong> a nombre del IDRD
                                       <ul>
                                          <li>Referencia 1 Código del parque y del servicio: <strong class="text-info">112100007</strong>
                                          <li>
                                             Referencia 2 Número de Identificación del niño o de la niña que toma el servicio.
                                             <servicio class=""></servicio>
                                       </ul>-->
                                 </div>
                              </div>
                              <!--fin acodion 5-->

                               <!--acordion 6-->
                              <div class="card-header card-collapse" role="tab" id="headingsix">
                                 <h5 class="mb-0 panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapsesix" aria-expanded="false" aria-controls="collapsesix" style="font-weight: bolder;color: #5D4593">
                                    INSCRIPCIÓN Y DESCARGA FORMATO PRE-INSCRIPCIÓN
                                    <i class="nc-icon nc-minimal-down"></i>
                                    </a>
                                 </h5>
                              </div>
                              <div id="collapsesix" class="collapse" role="tabpanel" aria-labelledby="headingsix">
                                 <div class="card-body">




                                    <div class="row">

                                       
                                       <div class="col-md-6">


                                          <div class="card card-profile card-plain">
                                             <div class="card-avatar" id="danieles">
                                                
                                             </div>
                                             <div class="card-body">
                                                <a href="registro">
                                                   <div class="author">
                                                      <h5 class="card-title" style="font-weight: bolder;">Pre-inscripción Niños y Niñas de 5 a 12 años</h5>
                                                   </div>
                                                </a>
                                                <p class="card-description text-center" style="font-weight: bolder;">
                                                   Inscripción de niños nacidos los años<br> 2000-2001-2001-2003-2004-2005
                                                </p>
                                             </div>
                                            
                                          </div>
                                       </div>
                                      
                                       <div class="col-md-6">
                                          <div class="card card-profile card-plain">
                                             <div class="card-avatar" id="danieles2">
                                                
                                             </div>
                                             <div class="card-body">
                                                <a href="descarga">
                                                   <div class="author">
                                                      <h5 class="card-title" style="font-weight: bolder;">Descarque aquí formato de pre-inscripción</h5>
                                                   </div>
                                                </a>
                                                <p class="card-description text-center" style="font-weight: bolder;">
                                                   Descargue aquí la pre-inscrpción al curso de natación de alumnos antiguos  y nuevos.
                                                </p>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <!--fin acodion 6-->


                           </div>
                        
                     </div>
                  </div>
               </div>
            </div>
         </div>
   </div>


<div id="" class="section  text-center">
   <div class="container">
      <div class="row">
         <div class="col-lg-12 col-md-12">
           <a href="comunicados" class="btn btn-outline-info btn-round" style="font-size: 13px;"><i class="fa fa-play"></i>COMUNICADOS Y NOVEDADES</a>
           <a href="inscripciones" class="btn btn-outline-dark btn-round" style="font-size: 13px; color: black"><i class="fa fa-play"></i>PROCESO DE INSCRIPCIÓN</a>
           <a href="cronograma" class="btn btn-outline-info btn-round" style="font-size: 13px"><i class="fa fa-play"></i>CRONOGRAMA Y PROGRAMACIÓN</a>
           <a href="consultas" class="btn btn-outline-info btn-round" style="font-size: 13px"><i class="fa fa-play"></i>CONSULTAS Y DESCARGAS</a>
                     
         </div>
      </div>
   </div>
</div>
<center><img src="{{ asset('public/images/logo-morado.jpg') }}" heigth="15%"  width="15%" alt=""></center>

      </div>
   </div>
</div>
</div>
@stop

