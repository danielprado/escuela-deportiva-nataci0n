

@extends('master')
@section('content')

<div class="wrapper">
   <div class="page-header page-header-xs" data-parallax="true" style="background-image: url('{{ asset('public/images/pool.jpg') }}');">
      <div class="filter"></div>
      <div class="content-center">
         <div class="container">
            <div class="title-brand">
               <h1>
                  IDRD 2019
               </h1>
               
            </div>
            <h5 class="text-center" style="color:#ffffff">
               ESCUELA DEPORTIVA DE NATACIÓN COMPLEJO ACUÁTICO SIMÓN BOLIVAR
            </h5>
         </div>
      </div>
   </div>
   <div class="section profile-content">
      <div class="container">
         <div class="card-body">
            <div class="card-description">

        
 

   
     <!-- Background -->
<section class="section section-shaped section-lg ">
    <div id="download" class="shape bg-gradient-warning shape-style-1 shape-default">
    </div>
    <div class="container pt-lg pb-300">
        <div class="row text-center justify-content-center">
            <div class="col-lg-12">
                <h5 style="color: #d81631"><strong>UTILICE EL SIGUIENTE FORMULARIO PARA DESCARGAR EL
                    COMPROBANTE DE INSCRIPCIÓN</strong></h5>
            </div>
        </div>
    </div>
    <!-- SVG separator -->
</section>
<!-- Form -->
<form method="POST" action="descarga" class="repeater" id="form_gen" >

     {{csrf_field()}}
    <section class="section section-lg pt-lg-0 section-contact-us">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="container">
            <div class="row justify-content-center mt--300">
                <div class="col-lg-12">
                    <div class="card bg-gradient-secondary shadow">
                        <div class="card-body p-lg-5">
                            <div class="row">

                                <div class="col-md-12">
                                    <label style="font-size: 0.9em; font-weight:600; line-height: 1.5em"><span style="color: red;font-size: 16px;text-transform: capitalize;color:red">*</span>&nbsp;&nbsp;NÚMERO
                                        DE DOCUMENTO</label>
                                    <input required type="text" class="form-control" id="cedula" name="cedula">
                                    <br>
                                </div>

                                <div>
                                    <button type="submit" class="btn btn-info btn-round btn-block btn-lg"
                                            value="enviar">Consultar
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</form>
@if(isset($aleatorio))
<form method="POST" action="download" class="repeater" id="form_gen" target="_blank" >

     {{csrf_field()}}
    <section class="section section-lg pt-lg-0 section-contact-us">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="container">
            <div class="row justify-content-center mt--300">
                <div class="col-lg-12">
                    <div class="card bg-gradient-secondary shadow">
                        <div class="card-body p-lg-5">
                            <div class="row">
                              <input type="hidden" name="cedula" value="{{$cedula}}">
                                <div class="col-md-12">
                                    <label style="font-size: 0.9em; font-weight:600; line-height: 1.5em"><span style="color: red;font-size: 16px;text-transform: capitalize;color:red">*</span>&nbsp;&nbsp;Seleccione la fecha de nacimiento correcto del niñ@</label>
                                   <select  required name="respuestas_fc" id="respuestas_fc" class="form-control" >
                                <option value="">Seleccione</option>
                                @foreach ($aleatorio as $cualquiera)
                                <option value="{{ $cualquiera['id'] }}">{{ $cualquiera['fecha_nacimiento']}}</option>
                                @endforeach
                             </select>
                                    <br>
                                </div>

                                <div>
                                    <button type="submit" class="btn btn-info btn-round btn-block btn-lg"
                                            value="enviar">Descargar
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</form>
@endif
<!--fin consentimiento informado -->   

        


<div id="" class="section  text-center">
   <div class="container">
      <div class="row">
         <div class="col-md-12">
           <a href="comunicados" class="btn btn-outline-info btn-round" style="font-size: 12px"><i class="fa fa-play"></i>COMUNICADOS Y NOVEDADES</a>
           <a href="inscripciones" class="btn btn-outline-info btn-round" style="font-size: 12px"><i class="fa fa-play"></i>PROCESO DE INSCRIPCIÓN</a>
           <a href="cronograma" class="btn btn-outline-info btn-round" style="font-size: 12px"><i class="fa fa-play"></i>CRONOGRAMA Y PROGRAMACIÓN</a>
           <a href="consultas" class="btn btn-outline-info btn-round" style="font-size: 12px"><i class="fa fa-play"></i>CONSULTAS Y DESCARGAS</a>
                     
         </div>
      </div>
   </div>
</div>
<center><img src="{{ asset('public/images/Logo-rojo.png') }}" heigth="20%"  width="20%" alt=""></center>

      </div>
   </div>
</div>
</div>
@stop

