
@extends('master')
@section('content')
<div class="wrapper">
   <div class="page-header page-header-xs" data-parallax="true" style="background-image: url('{{ asset('public/images/pool.jpg') }}');">
      <div class="filter"></div>
      <div class="content-center">
         <div class="container">
            <div class="title-brand">
               <h1 style="color: #FFF">
                  IDRD 2020
               </h1>
            </div>
            <h5 class="text-center" style="color:#ffffff">
               ESCUELA DEPORTIVA DE NATACIÓN COMPLEJO ACUÁTICO SIMÓN BOLIVAR
              
            </h5>
         </div>
      </div>
   </div>
   <div class="section profile-content">

<div class="container">
  <h2>NOTICIAS DESTACADAS</h2>
  <!--<p>Semana del 1 al 29 de febrero de 2020</p>-->
<br>
  <div class="row">
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="/w3images/lights.jpg" target="_blank">
          <img src="{{ asset('public/images/3.jpg') }}" alt="Lights" style="width:100%">
          <div class="caption">
            <p style="color: #000">Lorem ipsum donec id elit non mi porta gravida at eget metus.Lorem ipsum donec id elit non mi porta gravida at eget metus.Lorem ipsum donec id elit non mi porta gravida at eget metus.Lorem ipsum donec id elit non mi porta gravida at eget metus.Lorem ipsum donec id elit non mi porta gravida at eget metus.Lorem ipsum donec id elit</p>
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="/w3images/nature.jpg" target="_blank">
          <img src="{{ asset('public/images/4.jpg') }}" alt="Nature" style="width:100%">
          <div class="caption">
            <p>Lorem ipsum donec id elit non mi porta gravida at eget metus.</p>
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="/w3images/fjords.jpg" target="_blank">
          <img src="{{ asset('public/images/5.jpg') }}" alt="Fjords" style="width:100%">
          <div class="caption">
            <p>Lorem ipsum donec id elit non mi porta gravida at eget metus.</p>
          </div>
        </a>
      </div>
    </div>
  </div>
</div>



   <div class="section profile-content">
      <div class="container">
         <div class="card-body">
            <div class="card-description">
              <div class="card">
                <!-- una columna-->
                      <div class="card-header">
                       Semana del 01 al 29 de febrero de 2020
                      </div>
                      <div class="card-body">
                        <blockquote class="blockquote mb-0">
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                          <footer class="blockquote-footer">Someone famous in <cite title="Source Title">Source Title</cite></footer>
                        </blockquote>
                      </div>
                    </div>
                <!--fin-->

       <!-- doble columna-->
                        <div class="row">
                          <div class="col-sm-6">
                            <div class="card">
                              <div class="card-body">
                                <h5 class="card-title">Special title treatment</h5>
                                <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                                <a href="#" class="btn btn-primary">Go somewhere</a>
                              </div>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="card">
                              <div class="card-body">
                                <h5 class="card-title">Special title treatment</h5>
                                <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                                <a href="#" class="btn btn-primary">Go somewhere</a>
                              </div>
                            </div>
                          </div>
                        </div>
     <!--fin de doble columna-->


            </div>
         </div>
   </div>









         <div id="" class="section  text-center">
            <div class="container">
               <div class="row">
                  <div class="col-lg-12 col-md-12"><br><br>
                      <a href="comunicados" class="btn btn-outline-info btn-round" style="font-size: 13px"><i class="fa fa-play"></i>COMUNICADOS Y NOVEDADES</a>
           <a href="inscripciones" class="btn btn-outline-info btn-round" style="font-size: 13px"><i class="fa fa-play"></i>PROCESO DE INSCRIPCIÓN</a>
           <a href="cronograma" class="btn btn-outline-info btn-round" style="font-size: 13px"><i class="fa fa-play"></i>CRONOGRAMA Y PROGRAMACIÓN</a>
           <a href="consultas" class="btn btn-outline-info btn-round" style="font-size: 13px"><i class="fa fa-play"></i>CONSULTAS Y DESCARGAS</a>
                  </div>
               </div>
            </div>
         </div>
         <center><img src="{{ asset('public/images/Logo-rojo.png') }}" heigth="15%"  width="15%" alt=""></center>
      
   </div>
</div>
</div>
@stop

