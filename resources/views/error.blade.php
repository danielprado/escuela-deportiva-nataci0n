@extends('master')                              

@section('content') 


        
  <div id="top" class="page-header section-dark" style="background-image: url('{{ asset('public/images/pool.jpg') }}')">
    <div class="filter"></div>
    <div class="content-center">
        <div class="container">
            <div class="title-brand">
                <h4>
                    {{$error}}
                </h4>
               
            </div>

            <h1 class="presentation-subtitle text-center">
                ESCUELA DE NATACIÓN COMPLEJO ACUÁTICO SIMÓN BOLÍVAR - 2020<br>
               
               <img src="{{ asset('public/images/Logo-rojo.png') }}" heigth="30%"  width="30%" alt="">
            </h1>
        </div>
    </div>
    <div class="moving-clouds" style="background-image: url('{{ asset('public/images/clouds.png') }}'); ">

    </div>
    <br>
    <h6 class="category category-absolute">
        @if( isset( $download ) )
            <a href="{{ route('to_print', ['id' => isset( $download->id ) ? $download->id : 0 ]) }}" class="btn btn-info top-menu" style="z-index: 99">Descargar Comprobante</a>
        @endif
    </h6>


</div>      
@stop


