<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <!--[if !mso]><!-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!--<![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title></title>
    <style type="text/css">
        .ReadMsgBody { width: 100%; background-color: #ffffff; }
        .ExternalClass { width: 100%; background-color: #ffffff; }
        .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div { line-height: 100%; }
        html { width: 100%; }
        body { -webkit-text-size-adjust: none; -ms-text-size-adjust: none; margin: 0; padding: 0; }
        table { border-spacing: 0; table-layout: auto; margin: 0 auto; }
        .yshortcuts a { border-bottom: none !important; }
        img:hover { opacity: 0.9 !important; }
        a { color: #3cb2d0; text-decoration: none; }
        .textbutton a { font-family: 'open sans', arial, sans-serif !important; }
        .btn-link a { color: #FFFFFF !important; }

        @media only screen and (max-width: 479px) {
            body { width: auto !important; font-family: 'Open Sans', Arial, Sans-serif !important;}
            .table-inner{ width: 90% !important; text-align: center !important;}
            .table-full { width: 100%!important; max-width: 100%!important; text-align: center !important;}
            /*gmail*/
            u + .body .full { width:100% !important; width:100vw !important;}
        }
    </style>
</head>

<body class="body">
<table class="full" width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td background="{{ asset('https://www.idrd.gov.co/SIM/Recreacionv2/public/Img/bg.jpg') }}" bgcolor="#494c50" valign="top" style="background-size: cover; background-position: center;">
            <table class="table-inner" align="center" width="700" style="max-width: 700px;" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td height="40"></td>
                </tr>
                <tr>
                    <td bgcolor="#FFFFFF" style="border-top-left-radius: 4px;border-top-right-radius: 4px;" align="center">
                        <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                            <tr>
                                <td height="50"></td>
                            </tr>
                            <!-- logo -->
                            <tr>
                                <td align="center" style="line-height: 0px;"><img style="display:block; line-height:0px; font-size:0px; border:0px; height: 30%;" src="{{ asset('https://www.idrd.gov.co/SIM/Recreacionv2/public/Img/Logo-rojo.png') }}" alt="I.D.R.D." /></td>
                            </tr>
                            <!-- end logo -->
                            <tr>
                                <td height="15"></td>
                            </tr>
                            <!-- slogan -->
                            <tr>
                                <td align="center" style="font-family: 'Open Sans', Arial, sans-serif; font-size:18px; color:#3b3b3b; text-transform:uppercase; letter-spacing:2px; font-weight: normal;">ESCUELA DEPORTIVA DE NATACIÓN COMPLEJO ACUÁTICO SIMÓN BOLIVAR</td>
                            </tr>
                            <!-- end slogan -->
                            <tr>
                                <td height="40"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center" bgcolor="#f3f3f3">


                        <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                            <div class="row">
            <div class="col-md-6">
              <div class="panel panel-blue">INFORMACIÓN DEL NIÑO</div><br>

              <table cellspacing="0" style="font-size: 10pt">

                <tr style="font-family: ‘Arial Black’, Gadget, sans-serif;">
                    <td height="20px" width="320px"><strong># de inscripción - Documento de identidad</strong></td>
                    <td style="color: #0087ff; font-size: 13pt">{{ isset( $formulario->id, $formulario->cedula )? "{$formulario->id} - {$formulario->cedula}" : '' }}</td>
                   
                </tr>
            
                <tr style="font-family: ‘Arial Black’, Gadget, sans-serif;">
                    <td height="20px" width="280px"><strong>Nombre completo</strong></td>
                     <td>{{ isset( $formulario->nombre_nino, $formulario->apellido_nino ) ? "{$formulario->nombre_nino} {$formulario->apellido_nino}" : '' }}</td>
                   
                </tr>
                <tr style="font-family: ‘Arial Black’, Gadget, sans-serif;">
                    <td height="20px" width="280px"><strong>Fecha de nacicmiento y edad cumplida</strong></td>
                    <td>{{ isset( $formulario->fecha_nacimiento, $formulario->edad ) ? "{$formulario->fecha_nacimiento} - {$formulario->edad} años" : '' }}</td>
                   
                </tr>
                <tr style="font-family: ‘Arial Black’, Gadget, sans-serif;">
                    <td height="20px" width="280px"><strong>Dirección de residencia</strong></td>
                    <td>{{ isset( $formulario->direccion_nino ) ? $formulario->direccion_nino : '' }}</td>
                   
                </tr>
                <tr style="font-family: ‘Arial Black’, Gadget, sans-serif;">
                    <td height="20px" width="280px"><strong>Eps</strong></td>
                    <td>{{ isset( $formulario->eps ) ? $formulario->eps : '' }}</td>
                   
                </tr>
                <tr style="font-family: ‘Arial Black’, Gadget, sans-serif;">
                    <td height="20px" width="280px"><strong>Localidad</strong></td>
                    <td>{{ isset( $localidad->localidad ) ? $localidad->localidad : '' }}</td>
                   
                </tr>
                <tr style="font-family: ‘Arial Black’, Gadget, sans-serif;">
                    <td height="20px" width="280px"><strong>Institución educativa</strong></td>
                    <td>{{ isset( $formulario->institucion) ? "{$formulario->institucion}" : '' }}</td>
                   
                </tr>
                <tr style="font-family: ‘Arial Black’, Gadget, sans-serif;">
                    <td height="20px" width="280px"><strong>Sector del colegio</strong></td>
                    <td>{{ isset( $formulario->sector_colegio ) ? $formulario->sector_colegio : '' }}</td>
                   
                </tr>
                </tbody>
              </table>
            </div>
   <BR>

    <div class="col-md-6">
       <div class="panel panel-purple">INFORMACIÓN DEL CURSO</div> <br>
         <table cellspacing="0" style="font-size: 10pt">

                <tr style="font-family: ‘Arial Black’, Gadget, sans-serif;">
                    <td height="20px" width="320px"><strong>CATEGORÍA</strong></td>
                    <td style="color: #0087ff; font-size: 13pt">{{ isset( $categoria->nombre_categoria) ? "{$categoria->nombre_categoria}" : '' }}</td>
                   
                </tr>

                <tr style="font-family: ‘Arial Black’, Gadget, sans-serif;">
                    <td height="20px" width="280px"><strong>Horario del curso</strong></td>
                    <td>{{ isset( $horario->horario) ? "{$horario->horario}" : '' }}</td>
                   
                </tr>
                <tr style="font-family: ‘Arial Black’, Gadget, sans-serif;">
                    <td height="20px" width="280px"><strong>Ciclo de inscripción</strong></td>
                    <td>{{ isset( $formulario->ciclo ) ? $formulario->ciclo : '' }}</td>
                   
                </tr>
            
                </tbody>
              </table>


  <BR>

    <div class="col-md-6">
       <div class="panel panel-green">INFORMACIÓN DEL ACUDIENTE O REPRESENTANTE LEGAL</div>  <br>
         <table cellspacing="0" style="font-size: 10pt">

                <tr style="font-family: ‘Arial Black’, Gadget, sans-serif;">
                    <td height="20px" width="320px"><strong>Nombre completo</strong></td>
                    <td>{{ isset( $formulario->nombre_acudiente ) ? $formulario->nombre_acudiente : '' }}</td>
                   
                </tr>

                <tr style="font-family: ‘Arial Black’, Gadget, sans-serif;">
                    <td height="20px" width="280px"><strong>Documento de identidad</strong></td>
                    <td>{{ isset( $formulario->cedula_acudiente ) ? $formulario->cedula_acudiente : '' }}</td>
                   
                </tr>

                <tr style="font-family: ‘Arial Black’, Gadget, sans-serif;">
                    <td height="20px" width="280px"><strong>Ocupación</strong></td>
                    <td>{{ isset( $formulario->ocupacion ) ? $formulario->ocupacion : '' }}</td>
                   
                </tr>
                <tr style="font-family: ‘Arial Black’, Gadget, sans-serif;">
                    <td height="20px" width="280px"><strong>Correo electrónico</strong></td>
                    <td>{{ isset( $formulario->mail ) ? $formulario->mail : '' }}</td>
                   
                </tr>
                <tr style="font-family: ‘Arial Black’, Gadget, sans-serif;">
                    <td height="20px" width="280px"><strong>Teléfono fijo - Celular</strong></td>
                   <td>{{ isset( $formulario->telefono, $formulario->celular )? "{$formulario->telefono} - {$formulario->celular}" : '' }}</td>
                   
                </tr>
                
            
                </tbody>
              </table>



    </div>
  </div>
                            <!-- content -->
                            <tr>
                                <td align="center" style="font-family: 'Open Sans', Arial, sans-serif; font-size:14px; color:#7f8c8d; line-height:29px;">
                                    'Hemos registrado satisfactoriamente los datos para participar en el congreso '
                                </td>
                            </tr>
                            <!-- end content -->
                            <tr>
                                <td height="50"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td bgcolor="#FFFFFF" style="border-bottom-left-radius: 4px;border-bottom-right-radius: 4px;" align="center">
                        <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                            <tr>
                                <td height="40"></td>
                            </tr>
                            <!-- button -->
                            <tr>
                                <td align="center">
                                    <table class="textbutton" align="center" border="0" cellspacing="0" cellpadding="0">
                                       <!-- <tr>
                                            <td class="btn-link" bgcolor="#3cb2d0" height="55" align="center" style="font-family: 'Open Sans', Arial, sans-serif; font-size:16px; color:#FFFFFF;font-weight: bold;padding-left: 25px;padding-right: 25px;border-radius:4px;"><a href="https://www.idrd.gov.co/SIM/Presentacion/">Ir al S.I.M.</a></td>
                                        </tr>-->
                                    </table>
                                </td>
                            </tr>
                            <!-- end button -->
                            <tr>
                                <td height="30"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="25"></td>
                </tr>
                <!-- copyright -->
                <tr>
                    <td align="center" style="font-family: 'Open Sans', Arial, sans-serif; font-size:13px; color:#ffffff;"> © 2020 Instituto Distrital de Recreación y Deportes. </td>
                </tr>
                <!-- end copyright -->
                <tr>
                    <td height="25"></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>

</html>