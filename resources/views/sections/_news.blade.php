<div id="news" class="section section-dark text-center">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <h2 class="title">Comunicados o Novedades</h2><br/>
                <p class="description">
                    Novedades diciembre de 2018 hasta enero 15 de 2019
                    <br>
                    <small>
                        Las Preinscripciones para nadadores antiguos (que finalizaron el ciclo 8-2018) estarán disponibles desde el
                        18 de diciembre de 2018 hasta el 14 de enero de 2019 por este medio, este proceso es para el ciclo 1-2019 en el menú
                        “Inscripciones”, si no lo realiza en las fechas programadas se liberara el cupo para un nadador Nuevo.
                        <br><br>
                        La fecha de inicio y finalización del primer ciclo del año 2019 aún no tiene fechas programadas, debido a esto tampoco
                        se ha informado sobre las fechas de Formalización de la Inscripción (entrega en físico nuevamente de documentación actualizada
                        y pagos) para los niños y niñas que realizaron la Preinscripción como antiguos según el punto 1, no está programada por no
                        contar aún con la programación para el año 2019, esperamos que a FINALES de enero 2019 sean publicadas por este medio o en
                        la Coordinación de la Escuela del Complejo Acuático Simón Bolívar, de igual forma si la formalización no se realiza en
                        las fechas que se indiquen se pierde el cupo y de igual forma será habilitado para un alumno nuevo....
                    </small>
                </p><br/>
                <a href="javascript:;" class="btn btn-info btn-round" data-toggle="modal" data-target="#more">Ver más</a>

            </div>
        </div>
    </div>
</div>