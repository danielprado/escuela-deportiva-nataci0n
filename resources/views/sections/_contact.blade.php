<div id="contact" class="section landing-section" >
    
<div class="section section-dark section-nucleo-icons"  style="background-image: url('{{ asset('public/images/fondo7.jpg') }}');">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-10 col-lg-12">
                            
                            <p class="description">
                             <h3 style="color: white"><i class="nc-icon nc-email-85"> </i>&nbsp;<strong>Contacto Escuela de Natación Complejo Acuático Simón Bolívar (CASB) – IDRD vigencia 2020</strong></h3><br/>

                              <strong><p style="font-size: 13pt; text-align: justify; color: white; font-weight: bolder;">♣ Lorena Castellanos:</strong> Auxiliar Atención al Cliente Escuela CASB-IDRD <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Contratista IDRD.<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Calle 63 No 45 - 00<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PBX 2210622/23 extensión 109<br><br>

                            <strong>♣ Licenciado Cesar Cortés:</strong> Coordinador General Escuela CASB-IDRD <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Contratista IDRD.<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Calle 63 No 45 - 00<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PBX 2210622/23 extensión 106<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Correo:cesar.cortes@idrd.gov.co<br><br>
                    
                            <strong>♣ Instituto Distrital de Recreación y Deporte: </strong><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Calle 63 No. 59A - 06 <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PBX: 660 5400 <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Horario de atención IDRD: lunes a viernes de 7:00 AM A 4:30 PM</p>
                            
                        </div>

                        <div class="col-lg-10 col-lg-12">
                           
                            <p class="description">
                             <h3 class="title"><i class="nc-icon nc-watch-time"></i><strong> Horarios de Atención Escuela Complejo Acuático Simón Bolívar (CASB) – IDRD  vigencia 2018</strong></h3><br/>
                            <strong><p style="font-size: 13pt; text-align: justify; color: white; font-weight: bolder;">♣ Horarios Regulares:</strong> <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;* martes y jueves de 8:00 a.m. a 12:00 m. <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;* miércoles y viernes de 2:00 pm a 4:00 pm<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;* sábados y domingos de 7:00 a.m. a 4:00 p.m.<br><br>

                            <strong>♣ Horarios para Formalización de Inscripciones (2do Momento) :</strong> <br><u><i>Horario disponible solo en las fechas Programadas para este proceso según cronograma del año</i> </u><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;* martes de 8:00 a.m. a 12.00 m. y de 2:00 pm a 6:00 p.m.<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;* miércoles de 8:00 a.m. a 12.00 m. y de 2:00 pm a 6:00 p.m.</p>
             
                           
                        </div>

                    </div>
                </div>
            </div>
        </div>