<footer class="footer section-dark">
    <div class="container">
        <div class="row">
            <nav class="footer-nav">
                <ul>
                    <li>
                        <a href="#top" class="top-menu"><i class="nc-icon nc-world-2"></i> Inicio</a>
                    </li>
                    <li>
                        <a href="#description" class="top-menu"><i class="nc-icon nc-calendar-60"></i> Fechas de Inicio</a>
                    </li>
                    <li>
                        <a href="#news" class="top-menu"><i class="nc-icon nc-bookmark-2"></i> Comunicados</a>
                    </li>
                    <li>
                        <a href="#contact" class="top-menu"><i class="nc-icon nc-alert-circle-i"></i> Contácto</a>
                    </li>
                </ul>
            </nav>
            <div class="credits ml-auto">
					<span class="copyright">
						© <script>document.write(new Date().getFullYear())</script>, hecho con <i class="fa fa-heart heart"></i> por el IDRD
					</span>
            </div>
        </div>
    </div>
</footer>