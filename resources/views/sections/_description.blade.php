<div id="description" class="section text-center">
    <div class="container">
        <h1></h1>
        <br>
        <div class="row">
            <div class="col-md-12 ml-auto mr-auto">
                <h5 class="description">
                    El Complejo cuenta con un grupo de tres escenarios que pueden ser vistos desde sus graderías para capacidad de 1500 personas; una piscina de clavados de cinco metros de profundidad, otra olímpica de 25 por 50 metros y una última de entrenamiento (semiolímpica) con la mitad del área, además de estos existe una piscina para niños.
                </h5>
            </div>
        </div>
        <br/><br/>
        <div class="row">
            <div class="col-md-6">
                <div class="info">
                    <div class="icon icon-info">
                        <i class="nc-icon nc-calendar-60"></i>
                    </div>
                    <div class="description">
                        <h4 class="info-title">Fecha de Inicialización</h4>
                        <p class="description">Ciclo I - 2019</p>
                        <a href="javascript:;" class="btn btn-link btn-info">No está programada</a>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="info">
                    <div class="icon icon-info">
                        <i class="nc-icon nc-calendar-60"></i>
                    </div>
                    <div class="description">
                        <h4 class="info-title">Fecha de Finalización</h4>
                        <p>Ciclo I - 2019</p>
                        <a href="javascript:;" class="btn btn-link btn-info">No está programada</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>