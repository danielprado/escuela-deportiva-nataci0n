

<nav class="navbar navbar-expand-md fixed-top navbar-transparent" color-on-scroll="500">
   <div class="container">
      <div class="navbar-translate">
         <button class="navbar-toggler navbar-toggler-right navbar-burger" type="button" data-toggle="collapse" data-target="#navbarToggler" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
         <span class="navbar-toggler-bar"></span>
         <span class="navbar-toggler-bar"></span>
         <span class="navbar-toggler-bar"></span>
         </button>
         <a class="navbar-brand" href="https://www.idrd.gov.co">IDRD</a>
      </div>
      <div class="collapse navbar-collapse" id="navbarToggler">
         <ul class="navbar-nav mr-auto">
            <li class="nav-item">
               <a href="welcome#top" class="nav-link top-menu" style="font-size: 15px"><i class="nc-icon nc-world-2"></i> Inicio</a>
            </li>
            <li class="nav-item">
               <a href="welcome#tabs" class="nav-link top-menu" style="font-size: 15px"><i class="nc-icon nc-alert-circle-i"></i> + información</a>
            </li>
            <li class="nav-item">
               <a href="reglamento" class="nav-link top-menu" style="font-size: 15px"><i class="nc-icon nc-alert-circle-i"></i> Reglamento y Requisitos</a>
            </li>
            <li class="nav-item">
               <a href="https://idrd.gov.co/SIM/FORMULARIOS/Natacion_Vacacional_2018/" target="_black" class="nav-link top-menu" style="font-size: 15px"><i class="nc-icon nc-umbrella-13" ></i>vacacionales</a>
            </li>
            <li class="nav-item">
               <a href="clavados" target="_black" class="nav-link top-menu" style="font-size: 15px"><i class="nc-icon nc-alert-circle-i"></i>clavados</a>
            </li>
            <!--<li class="nav-item">
               <a href="welcome#news" class="nav-link top-menu"><i class="nc-icon nc-bookmark-2"></i> Comunicados</a>
            </li>-->
            
            <!--<div class="nav-item dropdown">
               <a class="nav-link dropdown-toggle" data-toggle="dropdown" id="dropdownMenuButton" href="#pk" role="button" aria-haspopup="true" aria-expanded="false"><i class="nc-icon nc-alert-circle-i"></i>+ información</a>
               <ul class="dropdown-menu dropdown-info" aria-labelledby="dropdownMenuButton">
                  <li class="dropdown-header">Dropdown header</li>
                  <a class="dropdown-item" href="#pk">Action</a>
                  <a class="dropdown-item" href="#pk">Another action</a>
                  <a class="dropdown-item" href="#pk">Something else here</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="#pk">Separated link</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="#pk">Another separated link</a>
               </ul>
            </div>-->
            <li class="nav-item">
               <a href="welcome#contact" class="nav-link top-menu" style="font-size: 15px"><i class="nc-icon nc-alert-circle-i"></i> Contácto</a>
            </li>
         </ul>
      </div>
   </div>
</nav>

