<div  id ="tabs" class="section profile-content">
    <div class="container">
        <div class="owner">
            <div class="name">
                <h2 class="title" style="font-weight: bolder;">Más Información de la Escuela de Natación</h3>
            </div>
        </div><br>
        <div class="nav-tabs-navigation">
            <div class="nav-tabs-wrapper">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#objetivos" role="tab" style="font-size: 16pt; font-weight: bolder;"><strong>Objetivos</strong></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#poblacion" role="tab" style="font-size: 16pt; font-weight: bolder;">Población</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#estructura" role="tab" style="font-size: 16pt; font-weight: bolder;">Estructura y Lineamientos</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#cobertura" role="tab" style="font-size: 16pt; font-weight: bolder;">Cobertura y Cupos</a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- Tab panes -->
        <div class="tab-content ">

        <!--población-->
            <div class="tab-pane active text-justify" id="objetivos" role="tabpanel">
                <div class="row">
                    <div class="col-md-12 ml-auto mr-auto">
                        <h5 style=" color: #5D4593; font-weight: bolder;"><strong>OBJETIVO GENERAL</strong></h5>
                        <small  style="font-size: 13pt; text-align: justify;">
                            Garantizar un proceso de enseñanza en el Deporte de la Natación Clásica seguro y de calidad a través de un plan
                            pedagógico estructurado por Fases de Aprendizaje, desarrollado en niveles mediante cursos previamente programados,
                            para el desarrollo del deporte, desde la fundamentación hasta la tecnificación de los estilos logrando adaptación
                            al medio acuático, desarrollo de habilidades y corrección de los estilos.
                        </small ><br><br>
                        <h5 style=" color: #5D4593; font-weight: bolder;"><strong>OBJETIVOS ESPECÍFICOS</strong></h5>
                        <ul class="list-unstyled follows">
                            <li>
                                <div class="row">
                                    <div class="col-md-1 col-sm-1 ml-auto mr-auto">
                                        <i class="nc-icon nc-minimal-right"></i>
                                    </div>
                                    <div class="col-md-11 col-sm-7  ml-auto mr-auto">
                                        <small style="font-size: 13pt; text-align: justify;">Formar nadadores, para la ejecución eficiente de los distintos estilos de la natación clásica, aplicables a la especialización de las distintas modalidades acuáticas.</small>
                                    </div>
                                </div>
                            </li>
                            <hr />
                            <li>
                                <div class="row">
                                    <div class="col-md-1 col-sm-1 ml-auto mr-auto">
                                        <i class="nc-icon nc-minimal-right"></i>
                                    </div>
                                    <div class="col-md-11 col-sm-7  ml-auto mr-auto">
                                        <small style="font-size: 13pt; text-align: justify;">
                                            Desarrollar en los niños y niñas hábitos de vida saludable que contribuyan a su proyecto de vida.
                                        </small>
                                    </div>
                                </div>
                            </li>
                            <hr />
                            <li>
                                <div class="row">
                                    <div class="col-md-1 col-sm-1 ml-auto mr-auto">
                                        <i class="nc-icon nc-minimal-right"></i>
                                    </div>
                                    <div class="col-md-11 col-sm-7  ml-auto mr-auto">
                                        <small style="font-size: 13pt; text-align: justify;">
                                            Lograr autonomía en cada uno de los nadadores mediante el gusto y la diversión por el deporte.
                                        </small>
                                    </div>
                                </div>
                            </li>
                            <hr />
                            <li>
                                <div class="row">
                                    <div class="col-md-1 col-sm-1 ml-auto mr-auto">
                                        <i class="nc-icon nc-minimal-right"></i>
                                    </div>
                                    <div class="col-md-11 col-sm-7  ml-auto mr-auto">
                                        <small style="font-size: 13pt; text-align: justify;">
                                            Identificar y asesorar a los nadadores que por su destreza técnica están en capacidad de hacer parte del grupo de rendimiento deportivo.
                                        </small>
                                    </div>
                                </div>
                            </li>
                            <hr />
                            <li>
                                <div class="row">
                                    <div class="col-md-1 col-sm-1 ml-auto mr-auto">
                                        <i class="nc-icon nc-minimal-right"></i>
                                    </div>
                                    <div class="col-md-11 col-sm-7  ml-auto mr-auto">
                                        <small style="font-size: 13pt; text-align: justify;">
                                            Fortalecer el trabajo educativo y la formación de valores durante el proceso de formación del nadador y de esta manera contribuir en la convivencia ciudadana y        la paz de nuestro entorno.
                                        </small>
                                    </div>
                                </div>
                            </li>
                            <hr />
                        </ul>
                    </div>
                </div>
            </div>

            <!--población-->
            <div class="tab-pane" id="poblacion" role="tabpanel">
                <small style="font-size: 13pt; text-align: justify;">
                   <h6 style=" color: #5D4593; font-weight: bolder;"><strong>CURSOS REGULARES</strong></h6><br>
                    La Escuela de Natación del CASB – IDRD pretende ofrecer el servicio de enseñanza de habilidades básicas acuáticas y estilos de la natación clásica a niños y niñas residentes en la ciudad de Bogotá con edades comprendidas entre los 5 y los 12 años de edad (cumplidos); ubicados en grupos estratégicos por edad de acuerdo a sus habilidades y Capacidades, Cualidades y Características de Crecimiento y Desarrollo.<br><br>
                </small>
    
                <small style="font-size: 13pt; text-align: justify;">
                   <h6 style=" color: #5D4593; font-weight: bolder;"><strong>CURSOS VACACIONALES</strong></h6><br>
                    La Escuela de Natación del CASB – IDRD pretende ofrecer el servicio vacacional de habilidades básicas acuáticas y estilos de la natación clásica a los residentes de la ciudad de Bogotá con edades comprendidas entre los 4 y 59 años de edad (cumplidos); ubicados en grupos estratégicos por edad de acuerdo a sus habilidades y Capacidades, Cualidades y Características psicomotrices.
                </small>
            </div>


            <!-- estructura y linemanientos-->
            <div class="tab-pane text-justify" id="estructura" role="tabpanel">
                <ul class="list-unstyled follows">
                    <li>
                        <div class="row">
                            <div class="col-md-1 col-sm-1 ml-auto mr-auto">
                                <i class="nc-icon nc-minimal-right"></i>
                            </div>
                            <div class="col-md-11 col-sm-7  ml-auto mr-auto">
                                <small style="font-size: 13pt; text-align: justify;">
                                   <strong>Oferta de cursos solo fines de semana (sábados y Domingos) </strong> en un horario entre las 7:00 am a 4.00 pm (los horarios establecidos para cada inicio de           clases son: 7:00 a.m., 8:30 a.m., 10:00 a.m., 11:30 a.m., 1:30 p.m. y 3:00 p.m.).
                                </small>
                            </div>
                        </div>
                    </li>
                    <hr />
                    <li>
                        <div class="row">
                            <div class="col-md-1 col-sm-1 ml-auto mr-auto">
                                <i class="nc-icon nc-minimal-right"></i>
                            </div>
                            <div class="col-md-11 col-sm-7  ml-auto mr-auto">
                                <small style="font-size: 13pt; text-align: justify;">
                                    El Horario que escoge en el ciclo es igual para ambos días y no se puede cambiar una vez inscrito. 
                                </small>
                            </div>
                        </div>
                    </li>
                    <hr />
                    <li>
                        <div class="row">
                            <div class="col-md-1 col-sm-1 ml-auto mr-auto">
                                <i class="nc-icon nc-minimal-right"></i>
                            </div>
                            <div class="col-md-11 col-sm-7  ml-auto mr-auto">
                                <small style="font-size: 13pt; text-align: justify;">
                                     Cupos de nadadores(as) limitados por horario, grupo y profesor.
                                </small>
                            </div>
                        </div>
                    </li>
                    <hr />
                    <li>
                        <div class="row">
                            <div class="col-md-1 col-sm-1 ml-auto mr-auto">
                                <i class="nc-icon nc-minimal-right"></i>
                            </div>
                            <div class="col-md-11 col-sm-7  ml-auto mr-auto">
                                <small style="font-size: 13pt; text-align: justify;">
                                    Cada Ciclo consta de mínimo 8 clases o sesiones, lo que equivale a un mes aproximadamente según programación, cada clase o sesión es de 1 (una) hora de        duración (8 horas por Ciclo o Mes). Las fechas de Preinscripción (1er Momento) y Formalización (2do Momento) de cada ciclo las puede consultar en el Menú   <a href="cronograma" style="font-weight: bolder;">CRONOGRAMA Y PROGRAMACIÓN</a>
                                </small>
                            </div>
                            
                        </div>
                    </li>
                    <hr />
                    <li>
                        <div class="row">
                            <div class="col-md-1 col-sm-1 ml-auto mr-auto">
                                <i class="nc-icon nc-minimal-right"></i>
                            </div>
                            <div class="col-md-11 col-sm-7  ml-auto mr-auto">
                                <small style="font-size: 13pt; text-align: justify;">
                                     En Promedio se desarrollan Siete (7) cursos o ciclos anuales y uno (1) Vacacional.
                                </small>
                            </div>
                        </div>
                    </li>
                    <hr />
                    <li>
                        <div class="row">
                            <div class="col-md-1 col-sm-1 ml-auto mr-auto">
                                <i class="nc-icon nc-minimal-right"></i>
                            </div>
                            <div class="col-md-11 col-sm-7  ml-auto mr-auto">
                                <small style="font-size: 13pt; text-align: justify;">
                                    Curso con tarifas, según manual de aprovechamiento económico y resolución vigente, los valores para cada vigencia los puede consultar en el Menú <a href="inscripciones" style="font-weight: bolder;">INSCRIPCIONES</a>
                                </small>
                            </div>
                           <!-- <div class="col-md-3 col-sm-2  ml-auto mr-auto">
                                <btn class="btn btn-outline-info btn-round"><i class="fa fa-calendar"></i> Aquí</btn>
                            </div>-->
                        </div>
                    </li>
                    <hr />
                    <li>

                        <div class="row">
                            <div class="col-md-1 col-sm-1 ml-auto mr-auto">
                                <i class="nc-icon nc-minimal-right"></i>
                            </div>
                            <div class="col-md-11 col-sm-7  ml-auto mr-auto">
                                <small style="font-size: 13pt; text-align: justify;">
                                    El Plan Pedagógico y los Niveles que se Desarrollan en la Escuela se relacionan en el siguiente cuadro:
                                 <br><br>
                                 <!-- programa menores de 5-7 años-->
                                     <p>                            
                                      <div class="title-info" style="text-align: center;">
                                         <h6>PROGRAMA DE MENORES (5 a 7 años)</h6>
                                         <small style="color: #5D4593; font-weight: bolder">NOTA: clic sobre el nivel para consultar los objetivos de cada uno.</small>
                                      </div>

                                         <div class="container">
 
                                              <table class="table table-bordered">
                                                <thead>
                                                  <tr>
                                                    
                                                    <th>FASE</th>
                                                    <th>NIVELES</th>

                                                  </tr>
                                                </thead>
                                                <tbody>
                                                <!--familiarización-->
                                                  <tr>
                                                    <td rowspan="2" style="width: 40%;"><strong><br>FAMILIARIZACIÓN Y FLOTACIONES</strong></td>
                                                     <td>
                                                        
                                                       <a href="javascrip:;" data-toggle="popover" title="OBJETIVO GENERAL" data-content="Generar la máxima seguridad y confianza  del niño(a) dentro del agua,mediante habilidades básicas alcanzando la adaptación al medio acuático." style="font-weight: bolder;"><strong>ADAPTACIÓN 1</strong></a>                         
                                                     </td>
                                                  </tr>      
                                                  <tr>
                                                    <td> 
                                                        <a href="javascrip:;" data-toggle="popover" title="OBJETIVO GENERAL" data-content="Aumentar la confianza del niño(a) en el agua, mediante un conjunto de ejercicios que favorezca la flotación, respiración y deslizamiento en el medio acuático."style="font-weight: bolder;"><strong>ADAPTACION 2</strong></a>     
                                                    </td>
                                                
                                                  </tr>
                                               <!--exploración-->
                                                  <tr>
                                                    <td rowspan="2" style="width: 40%;"><strong><br>EXPLORACIÓN</strong></td>
                                                     <td>
                                                        
                                                       <a href="javascrip:;" data-toggle="popover" title="OBJETIVO GENERAL" data-content=" Coordinar movimientos de brazos y piernas  que le permitan desplazarse con facilidad."style="font-weight: bolder;"><strong>HABILIDADES 1</strong></a>                         
                                                     </td>
                                                  </tr>      
                                                  <tr>
                                                    <td> 
                                                        <a href="javascrip:;" data-toggle="popover" title="OBJETIVO GENERAL" data-content=" Coordinar movimientos de brazos y piernas alternos y simultáneos que le permitan desplazarse con facilidad."style="font-weight: bolder;"><strong>HABILIDADES 2</strong></a>     
                                                    </td>
                                                
                                                  </tr>
                                                  
                                                <!--patadas-->
                                                 <tr>
                                                    <td rowspan="2" style="width: 40%;"><strong><br>PATADAS Y BRAZADAS</strong></td>
                                                     <td>
                                                        
                                                       <a href="javascrip:;" data-toggle="popover" title="OBJETIVO GENERAL" data-content="Ejecutar desplazamientos con movimiento de brazos y piernas de manera simultánea y alterna incluyendo cambios de posición con adecuado ajuste corporal."style="font-weight: bolder;"><strong>DESPLAZAMIENTOS 1</strong></a>                         
                                                     </td>
                                                  </tr>      
                                                  <tr>
                                                    <td> 
                                                        <a href="javascrip:;" data-toggle="popover" title="OBJETIVO GENERAL" data-content="Integrar secuencias de movimientos simultáneos de brazos y piernas de forma eficiente que permita desplazamientos con un correcto ajuste corporal."style="font-weight: bolder;"><strong>DESPLAZAMIENTOS 2</strong></a>     
                                                    </td>
                                                
                                                  </tr>

                                                  <!--globalidades-->
                                                   <tr>
                                                    <td rowspan="2" style="width: 40%;"><strong><br>GLOBALIDADES</strong></td>
                                                     <td>
                                                        
                                                       <a href="javascrip:;" data-toggle="popover" title="OBJETIVO GENERAL" data-content="Ampliar los gestos motrices tendientes a los estilos alternos en distintas posiciones con adecuado ajuste corporal."style="font-weight: bolder;"><strong>MOVIMIENTOS ALTERNOS</strong></a>                         
                                                     </td>
                                                  </tr>      
                                                  <tr>
                                                    <td> 
                                                        <a href="javascrip:;" data-toggle="popover" title="OBJETIVO GENERAL" data-content="Ampliar los gestos motrices tendientes a los estilos simultáneos en distintas posiciones con adecuado ajuste corporal."style="font-weight: bolder;"><strong>MOVIMIENTOS SIMULTÁNEOS </strong></a>     
                                                    </td>
                                                
                                                  </tr>
                                                </tbody>
                                              </table>
                                        </div>
                                  </p>

                              <br>

                                <!-- programa menores de 8-12 años-->
                                     <p>                            
                                      <div class="title-info" style="text-align: center;">
                                         <h6>PROGRAMA INICIACIÓN A LA TÉCNICA (8 a 12 años)</h6><BR>
                                      </div>

                                         <div class="container">
 
                                              <table class="table table-bordered">
                                                <thead>
                                                  <tr>
                                                    
                                                    <th>FASE</th>
                                                    <th>NIVELES</th>

                                                  </tr>
                                                </thead>
                                                <tbody>
                                                <!--familiarización-->
                                                  <tr>
                                                    <td rowspan="2" style="width: 40%;"><strong><br>FAMILIARIZACIÓN Y FLOTACIONES</strong></td>
                                                     <td>
                                                        
                                                       <a href="javascrip:;" data-toggle="popover" title="OBJETIVO GENERAL" data-content="Generar la máxima seguridad y confianza  del niño(a) dentro del agua,mediante habilidades básicas alcanzando la adaptación al medio acuático."style="font-weight: bolder;"><strong>AMBIENTACIÓN</strong></a>                         
                                                     </td>
                                                  </tr>      
                                                  <tr>
                                                    <td> 
                                                        <a href="javascrip:;" data-toggle="popover" title="OBJETIVO GENERAL" data-content="Aumentar la confianza del niño(a) en el agua, mediante un conjunto de ejercicios que favorezca la flotación, respiración y deslizamiento en el medio acuático."style="font-weight: bolder;"><strong>AMBIENTACIÓN AVANZADA</strong></a>     
                                                    </td>
                                                
                                                  </tr>

                                                     <!--globalidades-->
                                                  <tr>
                                                    <td rowspan="2" style="width: 40%;"><strong><br>GLOBALIDADES</strong></td>
                                                     <td>
                                                        
                                                       <a href="javascrip:;" data-toggle="popover" title="OBJETIVO GENERAL" data-content=" Coordinar movimientos de brazos y piernas alternos que le permitan desplazarse con facilidad."style="font-weight: bolder;"><strong>DESTREZAS ACUÁTICAS BÁSICAS</strong>                       
                                                     </td>
                                                  </tr>      
                                                  <tr>
                                                    <td> 
                                                        <a href="javascrip:;" data-toggle="popover" title="OBJETIVO GENERAL" data-content="Coordinar movimientos de brazos y piernas simultáneos que le permitan desplazarse con facilidad."style="font-weight: bolder;"><strong>DESTREZAS ACUÁTICAS AVANZADAS</strong></a>     
                                                    </td>
                                                
                                                  </tr>
                                                  
                                                <!--estilo espalda-->
                                                 <tr>
                                                    <td rowspan="2" style="width: 40%;"><strong><br>ESTILO ESPALDA</strong></td>
                                                     <td>
                                                        
                                                       <a href="javascrip:;" data-toggle="popover" title="OBJETIVO GENERAL" data-content="Ejecutar desplazamientos con movimiento de brazos y piernas de manera simultánea y alterna incluyendo cambios de posición con adecuado ajuste corporal."style="font-weight: bolder;"><strong>ESPALDA</strong></a>                         
                                                     </td>
                                                  </tr>      
                                                  <tr>
                                                    <td> 
                                                        <a href="javascrip:;" data-toggle="popover" title="OBJETIVO GENERAL" data-content="Integrar secuencias de movimientos simultáneos de brazos y piernas de forma eficiente que permita desplazamientos con un correcto ajuste corporal."style="font-weight: bolder;"><strong>ESPALDA AVANZADO</strong></a>     
                                                    </td>
                                                
                                                  </tr>

                                                  <!--estilo libre-->
                                                   <tr>
                                                    <td rowspan="2" style="width: 40%;"><strong><br>ESTILO LIBRE</strong></td>
                                                     <td>
                                                        
                                                       <a href="javascrip:;" data-toggle="popover" title="OBJETIVO GENERAL" data-content="Ampliar los gestos motrices tendientes a los estilos alternos en distintas posiciones con adecuado ajuste corporal."style="font-weight: bolder;"><strong>LIBRE</strong></a>                         
                                                     </td>
                                                  </tr>      
                                                  <tr>
                                                    <td> 
                                                        <a href="javascrip:;" data-toggle="popover" title="OBJETIVO GENERAL" data-content="Ampliar los gestos motrices tendientes a los estilos simultáneos en distintas posiciones con adecuado ajuste corporal."style="font-weight: bolder;"><strong>LIBRE AVANZADO</strong></a>     
                                                    </td>
                                                
                                                  </tr>

                                                  <!--estilo PECHO-->
                                                   <tr>
                                                    <td rowspan="2" style="width: 40%;"><strong><br>ESTILO PECHO</strong></td>
                                                     <td>
                                                        
                                                       <a href="javascrip:;" data-toggle="popover" title="OBJETIVO GENERAL" data-content="Ampliar los gestos motrices tendientes a los estilos alternos en distintas posiciones con adecuado ajuste corporal."style="font-weight: bolder;"><strong>PECHO</strong></a>                         
                                                     </td>
                                                  </tr>      
                                                  <tr>
                                                    <td> 
                                                        <a href="javascrip:;" data-toggle="popover" title="OBJETIVO GENERAL" data-content="Ampliar los gestos motrices tendientes a los estilos simultáneos en distintas posiciones con adecuado ajuste corporal."style="font-weight: bolder;"><strong>PECHO AVANZADO</strong></a>     
                                                    </td>
                                                
                                                  </tr>

                                                  <!--estilo MARIPOSA-->
                                                   <tr>
                                                    <td rowspan="2" style="width: 40%;"><strong><br>ESTILO MARIPOSA</strong></td>
                                                     <td>
                                                        
                                                       <a href="javascrip:;" data-toggle="popover" title="OBJETIVO GENERAL" data-content="Ampliar los gestos motrices tendientes a los estilos alternos en distintas posiciones con adecuado ajuste corporal."style="font-weight: bolder;"><strong>MARIPOSA</strong></a>                         
                                                     </td>
                                                  </tr>      
                                                  <tr>
                                                    <td> 
                                                        <a href="javascrip:;" data-toggle="popover" title="OBJETIVO GENERAL" data-content="Ampliar los gestos motrices tendientes a los estilos simultáneos en distintas posiciones con adecuado ajuste corporal."style="font-weight: bolder;"><strong>MARIPOSA AVANZADO</strong></a>     
                                                    </td>
                                                
                                                  </tr>

                                                  <!--CULMINACIÓN-->
                                                   <tr>
                                                    <td rowspan="2" style="width: 40%;"><strong><br>CULMINACIÓN</strong></td>
                                                     <td>
                                                        
                                                       <a href="javascrip:;" data-toggle="popover" title="OBJETIVO GENERAL" data-content="Ampliar los gestos motrices tendientes a los estilos alternos en distintas posiciones con adecuado ajuste corporal."style="font-weight: bolder;"><strong>COMBINADO 1</strong></a>                         
                                                     </td>
                                                  </tr>      
                                                  <tr>
                                                    <td> 
                                                        <a href="javascrip:;" data-toggle="popover" title="OBJETIVO GENERAL" data-content="Ampliar los gestos motrices tendientes a los estilos simultáneos en distintas posiciones con adecuado ajuste corporal."style="font-weight: bolder;"><strong>COMBINADO 2</strong></a>     
                                                    </td>
                                                
                                                  </tr>

                                                   <!--globalidades-->
                                                   <tr>
                                                    <td rowspan="2" style="width: 40%;"><strong><br>GLOBALIDADES</strong></td>
                                                     <td>
                                                        
                                                       <a href="javascrip:;" data-toggle="popover" title="OBJETIVO GENERAL" data-content="Ampliar los gestos motrices tendientes a los estilos alternos en distintas posiciones con adecuado ajuste corporal."style="font-weight: bolder;"><strong>DESTREZAS ACUÁTICAS BÁSICA</strong></a>                         
                                                     </td>
                                                  </tr>      
                                                  <tr>
                                                    <td> 
                                                        <a href="javascrip:;" data-toggle="popover" title="OBJETIVO GENERAL" data-content="Ampliar los gestos motrices tendientes a los estilos simultáneos en distintas posiciones con adecuado ajuste corporal."style="font-weight: bolder;"><strong>DESTREZAS ACUÁTICAS AVANZADAS</strong></a>     
                                                    </td>
                                                
                                                  </tr>

                                                  
                                                </tbody>
                                              </table>
                                        </div>
                                  </p>



                                </small>
                            </div>
                        </div>
                    </li>
                    <hr />
                </ul>
                <small style="font-size: 13pt; text-align: justify; font-weight: bolder; text-align: justify;">
                   <strong> *</strong>   Al finalizar la fase de Globalidades en el Programa de Menores (5 a 7 años), los niños continúan al programa de Iniciación a la Técnica (8 a 12 años) en las fases de Globalidades o Espalda dependiendo de las habilidades y la edad de los niños o niñas, según la gráfica anterior. Los niños del programa de menores continúan al programa de iniciación a la técnica por evaluación del docente según edad y/o habilidad. Para mantener la progresión en los niveles se debe mantener la continuidad durante cada ciclo.
                </small>
            </div>

            <!--cobertura-->
            <div class="tab-pane text-justify" id="cobertura" role="tabpanel">
                <small style="font-size: 13pt; text-align: justify; font-weight: bolder;">
                    <h6 style=" color: #5D4593; font-weight: bolder;"><strong>CURSOS REGULARES</strong></h6><br>
                    * La Escuela del CASB-IDRD tiene una cobertura Máxima para 1.100 niños, niñas y adolescentes entre los 5 y 12 años de edad en todos los niveles de la estructura pedagógica y los horarios disponibles para los cupos regulares. Los cupos para niños o niñas ofrecidos una vez termine cada ciclo para el siguiente se ofertan según la disponibilidad de cupos habilitada para cada horario y nivel.<br><br>


                    <h6 style=" color: #5D4593; font-weight: bolder;"><strong>CURSOS VACACIONALES</strong></h6> 

                    * La Escuela de Natación CASB -IDRD  para los ciclos vacacionales y la disponibilidad de cupos que se habilitan dependen de la progrmación, las modalidades, las edades y los horarios disponibles.
                </small>
            </div>
        </div>
    </div>
</div>