
@extends('master')
@section('content')
<div class="wrapper">
   <div class="page-header page-header-xs" data-parallax="true" style="background-image: url('{{ asset('public/images/pool.jpg') }}');">
      <div class="filter"></div>
      <div class="content-center">
         <div class="container">
            <div class="title-brand">
               <h1>
                  IDRD 2020
               </h1>
            </div>
            <h5 class="text-center" style="color:#ffffff; font-weight: bolder;">
               ESCUELA DEPORTIVA DE NATACIÓN COMPLEJO ACUÁTICO SIMÓN BOLIVAR
              
            </h5>
         </div>
      </div>
   </div>
   <div class="section profile-content">
      <div class="container">
         <div class="card-body">
          
            <div class="card-description">
               <div id="acordeon">
                  <div id="accordion" role="tablist" aria-multiselectable="true">
                     <div class="card no-transition">
                        <!--reglamentación-->
                        <div class="card-header card-collapse" role="tab" id="headingOne">
                           <h5 class="mb-0 panel-title" >
                              <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne" style="font-weight: bolder;">
                              PROGRAMACIÓN DE DOCENTES
                              <i class="nc-icon nc-minimal-down"></i>
                              </a>
                           </h5>
                        </div>
                        <div id="collapseOne" class="collapse" role="tabpanel" aria-labelledby="headingOne">
                           <div class="card-body">
                              <p style="font-size: 18px; text-align: justify;">
                                La Escuela de Natación del CASB – IDRD pretende ofrecer el servicio de enseñanza de habilidades básicas acuáticas y estilos de la natación clásica a niños y niñas residentes en la ciudad de Bogotá con edades comprendidas entre los 5 y los 12 años de edad (cumplidos); ubicados en grupos estratégicos por edad de acuerdo a sus habilidades y Capacidades, Cualidades y Características de Crecimiento y Desarrollo.
                            </p>
                           </div>
                        </div>

                        <!--requisitos-->
                        <div class="card-header card-collapse" role="tab" id="headingTwo">
                           <h5 class="mb-0 panel-title">
                              <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo" style="font-weight: bolder;">
                              HISTÓRICO DE NIVELES DE LOS NADADORES
                              <i class="nc-icon nc-minimal-down"></i>
                              </a>
                           </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
                           <div class="card-body">

                              <!--acordion 12 - cupos-->
                              <div class="card-header card-collapse" role="tab" id="headingdoce">
                                 <h5 class="mb-0 panel-title" style="font-size: 18px" align="justify">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapsedoce" aria-expanded="false" aria-controls="collapsedoce" style="font-weight: bolder;">
                                    Cupos
                                    <i class="nc-icon nc-minimal-down"></i>
                                    </a>
                                 </h5>
                              </div>
                              <div id="collapsedoce" class="collapse" role="tabpanel" aria-labelledby="headingdoce">
                                 <div class="card-body">
                                    
                                    <h5 class="text-info" align="justify" style="font-size: 16px;font-weight: bolder;"><strong>Cupos mínimos y máximos para oferta de niveles</strong></h5>
                                    <p style="line-height: 20px; font-size: 11pt" align="justify">Los cupos mínimos y máximos para desarrollar las acciones pedagógicas de la escuela se definen por las inscripciones que se realizan mes a mes por parte de los padres de familia para el ciclo programado la cual se deriva en la organización de espacios y docentes:
                                       <br><br>
                                       La oferta y desarrollo de actividades pedagógicas de la Escuela en cada uno de los niveles, deberá tener como mínimo la inscripción y participación de ocho (8) niños por horario establecido; en caso de no contar con el número establecido, la Escuela podrá integrar grupos de niveles, establecer los horarios y/o aplazar el nivel para el correspondiente ciclo, siempre y cuando el número mínimo no se complete.
                                       <br><br>
                                       La oferta y desarrollo de actividades pedagógicas de la Escuela en cada uno de los niveles deberá tener como máximo la inscripción y participación en promedio de doce (12) niños, por horario establecido. 
                                    </p>
                                 </div>
                              </div>


                           </div>
                        </div>
                        <div class="card-header card-collapse" role="tab" id="headingThree">
                           <h5 class="mb-0 panel-title">
                              <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree" style="font-weight: bolder;">
                              DESCARGAS y ACTUALIZACIÓN DE DATOS 
                              <i class="nc-icon nc-minimal-down"></i>
                              </a>
                           </h5>
                        </div>
                        <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                           <div class="card-body" align="justify">
                              
<!--descargas-->

                                    <body>
                                        <p style="font-size: 18px; text-align: justify;">
                                           A través de este menú podrá descargar  los certificados de grado, el carnet y actualizar los datos de los niños y niñas de la Escuela de Natacaión CASB.
                                        </p><br>
                                        <div class="container">
                                         
                                            <div class="row">
                                                <div class="col-md-12">
                                                    
                                        
                                                    <!-- Tabs with icons on Card -->
                                                    <div class="card card-nav-tabs">
                                                        <div class="card-header card-header-primary">
                                                            <!-- colors: "header-primary", "header-info", "header-success", "header-warning", "header-danger" -->
                                                            <div class="nav-tabs-navigation">
                                                                <div class="nav-tabs-wrapper">
                                                                    <ul class="nav nav-tabs" data-tabs="tabs">
                                                                        <li class="nav-item">
                                                                            <a class="nav-link" href="#profile" data-toggle="tab" style="text-align: left; font-weight: bolder">
                                                                                
                                                                               Descargue aquí certificados
                                                                            </a>
                                                                        </li>
                                                                        <li class="nav-item">
                                                                            <a class="nav-link" href="#messages" data-toggle="tab" style="text-align: left; font-weight: bolder">
                                                                                
                                                                            Descargue aquí carnet
                                                                            </a>
                                                                        </li>
                                                                        <li class="nav-item">
                                                                            <a class="nav-link" href="#settings" data-toggle="tab" style="text-align: left; font-weight: bolder">
                                                                                
                                                                               Actualiza los datos del nadador 
                                                                            </a>
                                        
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div><div class="card-body ">
                                                            <div class="tab-content text-center">
                                                                <div class="tab-pane" id="profile">


                                    <!--nav descargas-->
                                                                           <div class="container">

                                                                                
                                                                                <div class="row">
                                                                                    <div class="col-md-12">
                                                                                        <!--<h3><small>Tabs with Icons on Card</small></h3>
                                                                            
                                                                                        <!-- Tabs with icons on Card -->
                                                                                        <div class="card card-nav-tabs">
                                                                                            <div class="card-header card-header-primary">
                                                                                                <!-- colors: "header-primary", "header-info", "header-success", "header-warning", "header-danger" -->
                                                                                                <div class="nav-tabs-navigation">
                                                                                                    <div class="nav-tabs-wrapper">
                                                                                                        <ul class="nav nav-tabs" data-tabs="tabs">
                                                                                                            <li class="nav-item">
                                                                                                                <a class="nav-link" href="#profile1" data-toggle="tab" style="text-align: left; font-weight: bolder">
                                                                                                                    
                                                                                                                  Certificado Graduados
                                                                                                                </a>
                                                                                                            </li>
                                                                                                            <li class="nav-item">
                                                                                                                <a class="nav-link" href="#messages1" data-toggle="tab" style="text-align: left; font-weight: bolder">
                                                                                                                    
                                                                                                                   Certificado Semilleros
                                                                                                                </a>
                                                                                                            </li>
                                                                                                            
                                                                                                        </ul>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div><div class="card-body ">
                                                                                                <div class="tab-content text-center">
                                                                                                    <div class="tab-pane" id="profile1">
                                                                                                        <p> POR MEDIO DE ESTE MENÚ PUEDE DESCARGAR EL CERTIFICADO DE GRADUADO DE LA ESCUELA DE NATACIÓN CASB - IDRD.<br>

                                                                                                            Recuerde que este proceso es para las niñas y niños que finalizaron todo sus niveles. Solo esta habiltado para los niños y niñas que culminaron su proceso en el año... </p>

                                                                                                         <!-- Form -->
                                                                                                         <form method="POST" action="algo" class="repeater" id="form_gen" >

                                                                                                              {{csrf_field()}}
                                                                                                             <section class="section section-lg pt-lg-0 section-contact-us">
                                                                                                                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                                                                                 <div class="container">
                                                                                                                     <div class="row justify-content-center mt--300">
                                                                                                                         <div class="col-lg-12">
                                                                                                                             <div class="card bg-gradient-secondary shadow">
                                                                                                                                 <div class="card-body p-lg-5">
                                                                                                                                     <div class="row">

                                                                                                                                         <div class="col-md-12">
                                                                                                                                             <label style="font-size: 0.9em; font-weight:600; line-height: 1.5em; text-align: left;"><span style="color: red;font-size: 16px;text-transform: capitalize;color:red">*</span>&nbsp;&nbsp;DIGITE EL NÚMERO DE DOCUMENTO DEL NIÑO O NIÑA</label>
                                                                                                                                             <input required type="text" class="form-control" id="cedula" name="cedula">
                                                                                                                                             <br>
                                                                                                                                         </div>

                                                                                                                                         <div>
                                                                                                                                             <button type="submit" class="btn btn-info btn-round btn-block btn-lg"
                                                                                                                                                     value="enviar">Consultar
                                                                                                                                             </button>
                                                                                                                                         </div>
                                                                                                                                     </div>
                                                                                                                                 </div>
                                                                                                                             </div>
                                                                                                                         </div>
                                                                                                                     </div>
                                                                                                                 </div>
                                                                                                             </section>
                                                                                                         </form>
                                                                                                    </div>
                                                                                                    <div class="tab-pane" id="messages1">
                                                                                                      <p> POR MEDIO DE ESTE MENÚ PUEDE DESCARGAR EL CERTIFICADO DE PARTICIPACION EN EL SEMILLERO DE LA ESCUELA DE NATACIÓN CASB - IDRD <br>

                                                                                                         Recuerde que este proceso es para las niñas y niños que finalizaron el proceso del Semillero. Solo esta habilitado para los niños y niñas que estuvieron vinculados a este espacio de la Escuela y culminaron su proceso de tres meses en el mismo, en el año 2017 y 2018. </p>

                                                                                                         <!-- Form -->
                                                                                                         <form method="POST" action="algo" class="repeater" id="form_gen" >

                                                                                                              {{csrf_field()}}
                                                                                                             <section class="section section-lg pt-lg-0 section-contact-us">
                                                                                                                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                                                                                 <div class="container">
                                                                                                                     <div class="row justify-content-center mt--300">
                                                                                                                         <div class="col-lg-12">
                                                                                                                             <div class="card bg-gradient-secondary shadow">
                                                                                                                                 <div class="card-body p-lg-5">
                                                                                                                                     <div class="row">

                                                                                                                                         <div class="col-md-12">
                                                                                                                                             <label style="font-size: 0.9em; font-weight:600; line-height: 1.5em; text-align: left;"><span style="color: red;font-size: 16px;text-transform: capitalize;color:red">*</span>&nbsp;&nbsp;DIGITE EL NÚMERO DE DOCUMENTO DEL NIÑO O NIÑA</label>
                                                                                                                                             <input required type="text" class="form-control" id="cedula" name="cedula">
                                                                                                                                             <br>
                                                                                                                                         </div>

                                                                                                                                         <div>
                                                                                                                                             <button type="submit" class="btn btn-info btn-round btn-block btn-lg"
                                                                                                                                                     value="enviar">Consultar
                                                                                                                                             </button>
                                                                                                                                         </div>
                                                                                                                                     </div>
                                                                                                                                 </div>
                                                                                                                             </div>
                                                                                                                         </div>
                                                                                                                     </div>
                                                                                                                 </div>
                                                                                                             </section>
                                                                                                         </form>
                                                                                                    </div>
                                                                                                    
                                                                                                </div>
                                                                                            </div></div>
                                                                                        <!-- End Tabs with icons on Card -->
                                                                            
                                                                                    </div>

                                                                                </div>
                                                                            </div>
   

                                                                </div>



                                                                <div class="tab-pane" id="messages">
                                                                    <p> I think that’s a responsibility that I have, to push possibilities, to show people, this is the level that things could be at. I will be the leader of a company that ends up being worth billions of dollars, because I got the answers. I understand culture. I am the nucleus. I think that’s a responsibility that I have, to push possibilities, to show people, this is the level that things could be at.</p>
                                                                </div>
                                                                <div class="tab-pane" id="settings">
                                                                    <p>I think that’s a responsibility that I have, to push possibilities, to show people, this is the level that things could be at. So when you get something that has the name Kanye West on it, it’s supposed to be pushing the furthest possibilities. I will be the leader of a company that ends up being worth billions of dollars, because I got the answers. I understand culture. I am the nucleus.</p>
                                                                </div>
                                                            </div>
                                                        </div></div>
                                                    <!-- End Tabs with icons on Card -->
                                        
                                                </div>
                                               

                                            </div>
                                        </div>
                                        

                                        
                                       

                                    </body>





                           </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div id="" class="section  text-center">
            <div class="container">
               <div class="row">
                  <div class="col-lg-12 col-md-12">
                      <a href="comunicados" class="btn btn-outline-info btn-round" style="font-size: 13px"><i class="fa fa-play"></i>COMUNICADOS Y NOVEDADES</a>
           <a href="inscripciones" class="btn btn-outline-info btn-round" style="font-size: 13px"><i class="fa fa-play"></i>PROCESO DE INSCRIPCIÓN</a>
           <a href="cronograma" class="btn btn-outline-info btn-round" style="font-size: 13px"><i class="fa fa-play"></i>CRONOGRAMA Y PROGRAMACIÓN</a>
           <a href="javascript:;" class="btn btn-outline-info btn-round" style="font-size: 13px"><i class="fa fa-play"></i>CONSULTAS Y DESCARGAS</a>
                  </div>
               </div>
            </div>
         </div>
         <center><img src="{{ asset('public/images/Logo-rojo.png') }}" heigth="20%"  width="20%" alt=""></center>
      </div>
   </div>
</div>
</div>
@stop

