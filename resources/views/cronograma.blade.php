
@extends('master')
@section('content')
<div class="wrapper">
   <div class="page-header page-header-xs" data-parallax="true" style="background-image: url('{{ asset('public/images/pool.jpg') }}');">
      <div class="filter"></div>
      <div class="content-center">
         <div class="container">
            <div class="title-brand">
               <h1>
                  IDRD 2020
               </h1>
            </div>
            <h5 class="text-center" style="color:#ffffff">
               ESCUELA DEPORTIVA DE NATACIÓN COMPLEJO ACUÁTICO SIMÓN BOLIVAR
              
            </h5>
         </div>
      </div>
   </div>
   <div class="section profile-content">
      
<div class="container">
        <div class="owner">
            <div class="name">
                <h3 class="title" style="font-weight: bolder;">Cronograma de la Escuela de Natación CASB<br/></h4>
            </div>
        </div>
        <div class="nav-tabs-navigation">
            <div class="nav-tabs-wrapper">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link " data-toggle="tab" href="#objetivos" role="tab" style="color: #000; font-weight: bolder;">Cronograma, Inscripciones y Ciclos</a>
                    </li>
                    <!--<li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#poblacion" role="tab" style="color: #000; font-weight: bolder;">Programa Docentes por Ciclo</a>
                    </li>
                    <!--<li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#estructura" role="tab" style="color: #0a80e8">Instructores Escuela de Natación CASB</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#cobertura" role="tab" style="color: #51bcda">Cobertura y Cupos</a>
                    </li>-->
                </ul>
            </div>
        </div>


       


        <!-- Tab panes  cronograma-->
        <div class="tab-content ">
            <div class="tab-pane active text-center" id="objetivos" role="tabpanel">
            <!--alumnnos antiguos-->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="nav-tabs-navigation">
                                <div class="nav-tabs-wrapper">
                                    <ul id="tabs" class="nav nav-tabs" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" data-toggle="tab" href="#ciclo-1" role="tab" style="font-size: 13px; color: #000; font-weight: bolder;">Ciclo 1-2020</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#ciclo-2" role="tab" style="font-size: 13px; color: #000; font-weight: bolder;">Ciclo 2-2020</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#ciclo-3" role="tab" style="font-size: 13px; color: #000; font-weight: bolder;">Ciclo 3-2020</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#vacacional-I" role="tab" style="font-size: 13px; color: #000; font-weight: bolder;">Vacacional - I</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#ciclo-5" role="tab" style="font-size: 13px; color: #000; font-weight: bolder;">Ciclo 4-2020</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#ciclo-6" role="tab" style="font-size: 13px; color: #000; font-weight: bolder;">Ciclo 5-2020</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#ciclo-7" role="tab" style="font-size: 13px; color: #000; font-weight: bolder;">Ciclo 6-2020</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#ciclo-8" role="tab" style="font-size: 13px; color: #000; font-weight: bolder;">Ciclo 7-2020</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#vacacional-II" role="tab" style="font-size: 13px; color: #000; font-weight: bolder;">Vacacional - II</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div id="my-tab-content" class="tab-content text-center">
                            <!--ciclo 1-->
                                <div class="tab-pane active" id="ciclo-1" role="tabpanel">
                                 <!--información ciclo-->
                                      <p>                            
                                      <div class="tim-title">
                                         <h5 style="font-weight: bolder;">INFORMACIÓN FECHAS CICLO 1</h5>
                                      </div>

                                         <div class="container">
 
                                              <table class="table">
                                                <thead>
                                                  <tr>
                                                    <th><div class="icon icon-danger">
                                                <i class="nc-icon nc-calendar-60"></i>
                                              </div>Fecha de inicio del ciclo</th>
                                                    <th><div class="icon icon-danger">
                                                <i class="nc-icon nc-calendar-60"></i>
                                              </div>Fecha terminación del ciclo</th>
                                                    <th> <div class="icon icon-danger">
                                                <i class="nc-icon nc-laptop"></i>
                                              </div>Fecha publicación página web</th>
                                                  </tr>
                                                </thead>
                                                <tbody>
                                                  <tr>
                                                    <td><strong>12 de mayo de 2019</strong></td>
                                                    <td>12 de septiembre de 2019</td>
                                                    <td>12 de mayo de 2019</td>
                                                  </tr>      
                                                  <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                  </tr>
                                                 
                                                     
                                                </tbody>
                                              </table>
                                        </div>
                                  </p>

                                 <!--información nadadores-->
                                 <p>                            
                                      <div class="tim-title">
                                         <h5 style="font-weight: bolder;">FECHAS INSCRIPCIÓN NADADORES</h5>
                                      </div>

                                         <div class="container">
 
                                              <table class="table">
                                                <thead>
                                                  <tr>
                                                    <th></th>
                                                    <th>Fecha Pre-inscripción</th>
                                                    <th>Fecha Formalización</th>
                                                  </tr>
                                                </thead>
                                                <tbody>
                                                  <tr>
                                                    <td  class="table-active" style="font-weight: bolder;"><strong>NADADORES ANTIGUOS</strong></td>
                                                    <td>12 de mayo de 2019</td>
                                                    <td>12 de mayo de 2019</td>
                                                  </tr>      
                                                  <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                  </tr>
                                                  <tr>
                                                    <td  class="table-active" style="font-weight: bolder;"><strong>NADADORES NUEVOS</strong></td>
                                                    <td>12 de mayo de 2019</td>
                                                    <td>12 de mayo de 2019</td>
                                                  </tr>
                                                     
                                                </tbody>
                                              </table>
                                        </div>
                                  </p>
                                </div>

                            <!--ciclo 2-->
                                <div class="tab-pane" id="ciclo-2" role="tabpanel">
                                   <!--información ciclo-->
                                      <p>                            
                                      <div class="tim-title">
                                         <h5 style="font-weight: bolder;">INFORMACIÓN FECHAS CICLO 2</h5>
                                      </div>

                                         <div class="container">
 
                                              <table class="table">
                                                <thead>
                                                  <tr>
                                                    <th><div class="icon icon-danger">
                                                <i class="nc-icon nc-calendar-60"></i>
                                              </div>Fecha de inicio del ciclo</th>
                                                    <th><div class="icon icon-danger">
                                                <i class="nc-icon nc-calendar-60"></i>
                                              </div>Fecha terminación del ciclo</th>
                                                    <th> <div class="icon icon-danger">
                                                <i class="nc-icon nc-laptop"></i>
                                              </div>Fecha publicación página web</th>
                                                  </tr>
                                                </thead>
                                                <tbody>
                                                  <tr>
                                                    <td><strong>12 de mayo de 2019</strong></td>
                                                    <td>12 de septiembre de 2019</td>
                                                    <td>12 de mayo de 2019</td>
                                                  </tr>      
                                                  <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                  </tr>
                                                 
                                                     
                                                </tbody>
                                              </table>
                                        </div>
                                  </p>

                                 <!--información nadadores-->
                                 <p>                            
                                      <div class="tim-title">
                                         <h5 style="font-weight: bolder;">INFORMACIÓN NADADORES</h5>
                                      </div>

                                         <div class="container">
 
                                              <table class="table">
                                                <thead>
                                                  <tr>
                                                    <th></th>
                                                    <th>Fecha Pre-inscripción</th>
                                                    <th>Fecha Formalización</th>
                                                  </tr>
                                                </thead>
                                                <tbody>
                                                  <tr>
                                                    <td  class="table-active"><strong>NADADORES ANTIGUOS</strong></td>
                                                    <td>12 de mayo de 2019</td>
                                                    <td>12 de mayo de 2019</td>
                                                  </tr>      
                                                  <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                  </tr>
                                                  <tr>
                                                    <td  class="table-active"><strong>NADADORES NUEVOS</strong></td>
                                                    <td>12 de mayo de 2019</td>
                                                    <td>12 de mayo de 2019</td>
                                                  </tr>
                                                     
                                                </tbody>
                                              </table>
                                        </div>
                                  </p>
                                </div>

                             <!--ciclo 3-->
                                <div class="tab-pane" id="ciclo-3" role="tabpanel">
                                  <!--información ciclo-->
                                      <p>                            
                                      <div class="tim-title">
                                         <h5 style="font-weight: bolder;">INFORMACIÓN FECHAS CICLO 3</h5>
                                      </div>

                                         <div class="container">
 
                                              <table class="table">
                                                <thead>
                                                  <tr>
                                                    <th><div class="icon icon-danger">
                                                <i class="nc-icon nc-calendar-60"></i>
                                              </div>Fecha de inicio del ciclo</th>
                                                    <th><div class="icon icon-danger">
                                                <i class="nc-icon nc-calendar-60"></i>
                                              </div>Fecha terminación del ciclo</th>
                                                    <th> <div class="icon icon-danger">
                                                <i class="nc-icon nc-laptop"></i>
                                              </div>Fecha publicación página web</th>
                                                  </tr>
                                                </thead>
                                                <tbody>
                                                  <tr>
                                                    <td><strong>12 de mayo de 2019</strong></td>
                                                    <td>12 de septiembre de 2019</td>
                                                    <td>12 de mayo de 2019</td>
                                                  </tr>      
                                                  <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                  </tr>
                                                 
                                                     
                                                </tbody>
                                              </table>
                                        </div>
                                  </p>

                                 <!--información nadadores-->
                                 <p>                            
                                      <div class="tim-title">
                                         <h5 style="font-weight: bolder;">INFORMACIÓN NADADORES</h5>
                                      </div>

                                         <div class="container">
 
                                              <table class="table">
                                                <thead>
                                                  <tr>
                                                    <th></th>
                                                    <th>Fecha Pre-inscripción</th>
                                                    <th>Fecha Formalización</th>
                                                  </tr>
                                                </thead>
                                                <tbody>
                                                  <tr>
                                                    <td  class="table-active"><strong>NADADORES ANTIGUOS</strong></td>
                                                    <td>12 de mayo de 2019</td>
                                                    <td>12 de mayo de 2019</td>
                                                  </tr>      
                                                  <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                  </tr>
                                                  <tr>
                                                    <td  class="table-active"><strong>NADADORES NUEVOS</strong></td>
                                                    <td>12 de mayo de 2019</td>
                                                    <td>12 de mayo de 2019</td>
                                                  </tr>
                                                     
                                                </tbody>
                                              </table>
                                        </div>
                                  </p>
                                </div>

                                <!--ciclo vacacional I-->
                                <div class="tab-pane" id="vacacional-I" role="tabpanel">
                                   <!--información ciclo-->
                                      <p>                            
                                      <div class="tim-title">
                                         <h5 style="font-weight: bolder;">INFORMACIÓN FECHAS CICLO VACACIONAL I</h5>
                                      </div>

                                         <div class="container">
 
                                              <table class="table">
                                                <thead>
                                                  <tr>
                                                    <th><div class="icon icon-danger">
                                                <i class="nc-icon nc-calendar-60"></i>
                                              </div>Fecha de inicio del ciclo</th>
                                                    <th><div class="icon icon-danger">
                                                <i class="nc-icon nc-calendar-60"></i>
                                              </div>Fecha terminación del ciclo</th>
                                                    <th> <div class="icon icon-danger">
                                                <i class="nc-icon nc-laptop"></i>
                                              </div>Fecha publicación página web</th>
                                                  </tr>
                                                </thead>
                                                <tbody>
                                                  <tr>
                                                    <td><strong>12 de mayo de 2019</strong></td>
                                                    <td>12 de septiembre de 2019</td>
                                                    <td>12 de mayo de 2019</td>
                                                  </tr>      
                                                  <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                  </tr>
                                                 
                                                     
                                                </tbody>
                                              </table>
                                        </div>
                                  </p>

                                 <!--información nadadores-->
                                 <p>                            
                                      <div class="tim-title">
                                         <h5 style="font-weight: bolder;">INFORMACIÓN NADADORES</h5>
                                      </div>

                                         <div class="container">
 
                                              <table class="table">
                                                <thead>
                                                  <tr>
                                                    <th></th>
                                                    <th>Fecha Pre-inscripción</th>
                                                    <th>Fecha Formalización</th>
                                                  </tr>
                                                </thead>
                                                <tbody>
                                                  <tr>
                                                    <td  class="table-active"><strong>NADADORES ANTIGUOS</strong></td>
                                                    <td>12 de mayo de 2019</td>
                                                    <td>12 de mayo de 2019</td>
                                                  </tr>      
                                                  <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                  </tr>
                                                  <tr>
                                                    <td  class="table-active"><strong>NADADORES NUEVOS</strong></td>
                                                    <td>12 de mayo de 2019</td>
                                                    <td>12 de mayo de 2019</td>
                                                  </tr>
                                                     
                                                </tbody>
                                              </table>
                                        </div>
                                  </p>
                                </div>
                                
                                <!--ciclo 4-->
                                <div class="tab-pane" id="ciclo-5" role="tabpanel">
                                  
                                   <!--información ciclo-->
                                      <p>                            
                                      <div class="tim-title">
                                         <h5 style="font-weight: bolder;">INFORMACIÓN FECHAS CICLO 4</h5>
                                      </div>

                                         <div class="container">
 
                                              <table class="table">
                                                <thead>
                                                  <tr>
                                                    <th><div class="icon icon-danger">
                                                <i class="nc-icon nc-calendar-60"></i>
                                              </div>Fecha de inicio del ciclo</th>
                                                    <th><div class="icon icon-danger">
                                                <i class="nc-icon nc-calendar-60"></i>
                                              </div>Fecha terminación del ciclo</th>
                                                    <th> <div class="icon icon-danger">
                                                <i class="nc-icon nc-laptop"></i>
                                              </div>Fecha publicación página web</th>
                                                  </tr>
                                                </thead>
                                                <tbody>
                                                  <tr>
                                                    <td><strong>12 de mayo de 2019</strong></td>
                                                    <td>12 de septiembre de 2019</td>
                                                    <td>12 de mayo de 2019</td>
                                                  </tr>      
                                                  <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                  </tr>
                                                 
                                                     
                                                </tbody>
                                              </table>
                                        </div>
                                  </p>

                                 <!--información nadadores-->
                                 <p>                            
                                      <div class="tim-title">
                                         <h5 style="font-weight: bolder;">INFORMACIÓN NADADORES</h5>
                                      </div>

                                         <div class="container">
 
                                              <table class="table">
                                                <thead>
                                                  <tr>
                                                    <th></th>
                                                    <th>Fecha Pre-inscripción</th>
                                                    <th>Fecha Formalización</th>
                                                  </tr>
                                                </thead>
                                                <tbody>
                                                  <tr>
                                                    <td  class="table-active"><strong>NADADORES ANTIGUOS</strong></td>
                                                    <td>12 de mayo de 2019</td>
                                                    <td>12 de mayo de 2019</td>
                                                  </tr>      
                                                  <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                  </tr>
                                                  <tr>
                                                    <td  class="table-active"><strong>NADADORES NUEVOS</strong></td>
                                                    <td>12 de mayo de 2019</td>
                                                    <td>12 de mayo de 2019</td>
                                                  </tr>
                                                     
                                                </tbody>
                                              </table>
                                        </div>
                                  </p>
                                </div>

                               <!--ciclo 5-->
                                <div class="tab-pane" id="ciclo-6" role="tabpanel">
                                  
                                   <!--información ciclo-->
                                      <p>                            
                                      <div class="tim-title">
                                         <h5 style="font-weight: bolder;">INFORMACIÓN FECHAS CICLO 5</h5>
                                      </div>

                                         <div class="container">
 
                                              <table class="table">
                                                <thead>
                                                  <tr>
                                                    <th><div class="icon icon-danger">
                                                <i class="nc-icon nc-calendar-60"></i>
                                              </div>Fecha de inicio del ciclo</th>
                                                    <th><div class="icon icon-danger">
                                                <i class="nc-icon nc-calendar-60"></i>
                                              </div>Fecha terminación del ciclo</th>
                                                    <th> <div class="icon icon-danger">
                                                <i class="nc-icon nc-laptop"></i>
                                              </div>Fecha publicación página web</th>
                                                  </tr>
                                                </thead>
                                                <tbody>
                                                  <tr>
                                                    <td><strong>12 de mayo de 2019</strong></td>
                                                    <td>12 de septiembre de 2019</td>
                                                    <td>12 de mayo de 2019</td>
                                                  </tr>      
                                                  <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                  </tr>
                                                 
                                                     
                                                </tbody>
                                              </table>
                                        </div>
                                  </p>

                                 <!--información nadadores-->
                                 <p>                            
                                      <div class="tim-title">
                                         <h5 style="font-weight: bolder;">INFORMACIÓN NADADORES</h5>
                                      </div>

                                         <div class="container">
 
                                              <table class="table">
                                                <thead>
                                                  <tr>
                                                    <th></th>
                                                    <th>Fecha Pre-inscripción</th>
                                                    <th>Fecha Formalización</th>
                                                  </tr>
                                                </thead>
                                                <tbody>
                                                  <tr>
                                                    <td  class="table-active"><strong>NADADORES ANTIGUOS</strong></td>
                                                    <td>12 de mayo de 2019</td>
                                                    <td>12 de mayo de 2019</td>
                                                  </tr>      
                                                  <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                  </tr>
                                                  <tr>
                                                    <td  class="table-active"><strong>NADADORES NUEVOS</strong></td>
                                                    <td>12 de mayo de 2019</td>
                                                    <td>12 de mayo de 2019</td>
                                                  </tr>
                                                     
                                                </tbody>
                                              </table>
                                        </div>
                                  </p>
                                </div>

                                <!--ciclo 6-->
                                <div class="tab-pane" id="ciclo-7" role="tabpanel">
                                   <!--información ciclo-->
                                      <p>                            
                                      <div class="tim-title">
                                         <h5 style="font-weight: bolder;">INFORMACIÓN FECHAS CICLO 6</h5>
                                      </div>

                                         <div class="container">
 
                                              <table class="table">
                                                <thead>
                                                  <tr>
                                                    <th><div class="icon icon-danger">
                                                <i class="nc-icon nc-calendar-60"></i>
                                              </div>Fecha de inicio del ciclo</th>
                                                    <th><div class="icon icon-danger">
                                                <i class="nc-icon nc-calendar-60"></i>
                                              </div>Fecha terminación del ciclo</th>
                                                    <th> <div class="icon icon-danger">
                                                <i class="nc-icon nc-laptop"></i>
                                              </div>Fecha publicación página web</th>
                                                  </tr>
                                                </thead>
                                                <tbody>
                                                  <tr>
                                                    <td><strong>12 de mayo de 2019</strong></td>
                                                    <td>12 de septiembre de 2019</td>
                                                    <td>12 de mayo de 2019</td>
                                                  </tr>      
                                                  <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                  </tr>
                                                 
                                                     
                                                </tbody>
                                              </table>
                                        </div>
                                  </p>

                                 <!--información nadadores-->
                                 <p>                            
                                      <div class="tim-title">
                                         <h5 style="font-weight: bolder;">INFORMACIÓN NADADORES</h5>
                                      </div>

                                         <div class="container">
 
                                              <table class="table">
                                                <thead>
                                                  <tr>
                                                    <th></th>
                                                    <th>Fecha Pre-inscripción</th>
                                                    <th>Fecha Formalización</th>
                                                  </tr>
                                                </thead>
                                                <tbody>
                                                  <tr>
                                                    <td  class="table-active"><strong>NADADORES ANTIGUOS</strong></td>
                                                    <td>12 de mayo de 2019</td>
                                                    <td>12 de mayo de 2019</td>
                                                  </tr>      
                                                  <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                  </tr>
                                                  <tr>
                                                    <td  class="table-active"><strong>NADADORES NUEVOS</strong></td>
                                                    <td>12 de mayo de 2019</td>
                                                    <td>12 de mayo de 2019</td>
                                                  </tr>
                                                     
                                                </tbody>
                                              </table>
                                        </div>
                                  </p>
                                </div>

                                <!--ciclo 7-->
                                <div class="tab-pane" id="ciclo-8" role="tabpanel">
                                   <!--información ciclo-->
                                      <p>                            
                                      <div class="tim-title">
                                         <h5 style="font-weight: bolder;">INFORMACIÓN FECHAS CICLO 7</h5>
                                      </div>

                                         <div class="container">
 
                                              <table class="table">
                                                <thead>
                                                  <tr>
                                                    <th><div class="icon icon-danger">
                                                <i class="nc-icon nc-calendar-60"></i>
                                              </div>Fecha de inicio del ciclo</th>
                                                    <th><div class="icon icon-danger">
                                                <i class="nc-icon nc-calendar-60"></i>
                                              </div>Fecha terminación del ciclo</th>
                                                    <th> <div class="icon icon-danger">
                                                <i class="nc-icon nc-laptop"></i>
                                              </div>Fecha publicación página web</th>
                                                  </tr>
                                                </thead>
                                                <tbody>
                                                  <tr>
                                                    <td><strong>12 de mayo de 2019</strong></td>
                                                    <td>12 de septiembre de 2019</td>
                                                    <td>12 de mayo de 2019</td>
                                                  </tr>      
                                                  <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                  </tr>
                                                 
                                                     
                                                </tbody>
                                              </table>
                                        </div>
                                  </p>

                                 <!--información nadadores-->
                                 <p>                            
                                      <div class="tim-title">
                                         <h5 style="font-weight: bolder;">INFORMACIÓN NADADORES</h5>
                                      </div>

                                         <div class="container">
 
                                              <table class="table">
                                                <thead>
                                                  <tr>
                                                    <th></th>
                                                    <th>Fecha Pre-inscripción</th>
                                                    <th>Fecha Formalización</th>
                                                  </tr>
                                                </thead>
                                                <tbody>
                                                  <tr>
                                                    <td  class="table-active"><strong>NADADORES ANTIGUOS</strong></td>
                                                    <td>12 de mayo de 2019</td>
                                                    <td>12 de mayo de 2019</td>
                                                  </tr>      
                                                  <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                  </tr>
                                                  <tr>
                                                    <td  class="table-active"><strong>NADADORES NUEVOS</strong></td>
                                                    <td>12 de mayo de 2019</td>
                                                    <td>12 de mayo de 2019</td>
                                                  </tr>
                                                     
                                                </tbody>
                                              </table>
                                        </div>
                                  </p>
                                </div>

                                <!--ciclo  vacacional 2-->
                                <div class="tab-pane" id="vacacional-II" role="tabpanel">
                                   <!--información ciclo-->
                                      <p>                            
                                      <div class="tim-title">
                                         <h5 style="font-weight: bolder;">INFORMACIÓN FECHAS CICLO VACIONAL II</h5>
                                      </div>

                                         <div class="container">
 
                                              <table class="table">
                                                <thead>
                                                  <tr>
                                                    <th><div class="icon icon-danger">
                                                <i class="nc-icon nc-calendar-60"></i>
                                              </div>Fecha de inicio del ciclo</th>
                                                    <th><div class="icon icon-danger">
                                                <i class="nc-icon nc-calendar-60"></i>
                                              </div>Fecha terminación del ciclo</th>
                                                    <th> <div class="icon icon-danger">
                                                <i class="nc-icon nc-laptop"></i>
                                              </div>Fecha publicación página web</th>
                                                  </tr>
                                                </thead>
                                                <tbody>
                                                  <tr>
                                                    <td><strong>12 de mayo de 2019</strong></td>
                                                    <td>12 de septiembre de 2019</td>
                                                    <td>12 de mayo de 2019</td>
                                                  </tr>      
                                                  <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                  </tr>
                                                 
                                                     
                                                </tbody>
                                              </table>
                                        </div>
                                  </p>

                                 <!--información nadadores-->
                                 <p>                            
                                      <div class="tim-title">
                                         <h5 style="font-weight: bolder;">INFORMACIÓN NADADORES</h5>
                                      </div>

                                         <div class="container">
 
                                              <table class="table">
                                                <thead>
                                                  <tr>
                                                    <th></th>
                                                    <th>Fecha Pre-inscripción</th>
                                                    <th>Fecha Formalización</th>
                                                  </tr>
                                                </thead>
                                                <tbody>
                                                  <tr>
                                                    <td  class="table-active"><strong>NADADORES ANTIGUOS</strong></td>
                                                    <td>12 de mayo de 2019</td>
                                                    <td>12 de mayo de 2019</td>
                                                  </tr>      
                                                  <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                  </tr>
                                                  <tr>
                                                    <td  class="table-active"><strong>NADADORES NUEVOS</strong></td>
                                                    <td>12 de mayo de 2019</td>
                                                    <td>12 de mayo de 2019</td>
                                                  </tr>
                                                     
                                                </tbody>
                                              </table>
                                        </div>
                                  </p>
                                </div>
                            </div>
                        </div>              
                   </div>              
            </div>

            <!--programa por docentes por ciclo-->
            <div class="tab-pane" id="poblacion" role="tabpanel">
                <p class="text-muted">
                    La Escuela de Natación del CASB – IDRD pretende ofrecer el servicio de enseñanza de habilidades básicas acuáticas y estilos de la natación clásica a niños y niñas residentes en la ciudad de Bogotá con edades comprendidas entre los 5 y los 12 años de edad (cumplidos); ubicados en grupos estratégicos por edad de acuerdo a sus habilidades y Capacidades, Cualidades y Características de Crecimiento y Desarrollo.
                </p>
            </div>
        </div>
    </div>


         <div id="" class="section  text-center">
            <div class="container">
               <div class="row">
                  <div class="col-lg-12 col-md-12"><br><br>
                      <a href="comunicados" class="btn btn-outline-info btn-round" style="font-size: 13px"><i class="fa fa-play"></i>COMUNICADOS Y NOVEDADES</a>
           <a href="inscripciones" class="btn btn-outline-info btn-round" style="font-size: 13px"><i class="fa fa-play"></i>PROCESO DE INSCRIPCIÓN</a>
           <a href="cronograma" class="btn btn-outline-info btn-round" style="font-size: 13px"><i class="fa fa-play"></i>CRONOGRAMA Y PROGRAMACIÓN</a>
           <a href="consultas" class="btn btn-outline-info btn-round" style="font-size: 13px"><i class="fa fa-play"></i>CONSULTAS Y DESCARGAS</a>
                  </div>
               </div>
            </div>
         </div>
         <center><img src="{{ asset('public/images/Logo-rojo.png') }}" heigth="15%"  width="15%" alt=""></center>
      
   </div>
</div>
</div>
@stop

