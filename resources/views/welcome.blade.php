@extends('master')                              

@section('content')
    @include('sections._landing')

    <div class="main">
        {{--@include('sections._description')

        @include('sections._news')--}}

        @include('sections._tabs')

       {{-- @include('sections._carousel')--}}

        @include('sections._contact')
    </div>
    <div id="contactUsMap" class="big-map"></div>
    <!-- Modal -->
    <div class="modal fade" id="more" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title text-center" id="exampleModalLabel">Comunicados o Novedades</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <ul class="list-unstyled follows">
                        <li>
                            <div class="row">
                                <div class="col-md-1 col-sm-1 ml-auto mr-auto">
                                    <i class="nc-icon nc-minimal-right"></i>
                                </div>
                                <div class="col-md-11 col-sm-7  ml-auto mr-auto">
                                    <small>
                                        Las Preinscripciones para nadadores antiguos (que finalizaron el ciclo 8-2018) estarán disponibles desde el 18 de diciembre de 2018 hasta el 14 de enero de 2019 por este medio, este proceso es para el ciclo 1-2019 en el menú “Inscripciones”, si no lo realiza en las fechas programadas se liberara el cupo para un nadador Nuevo.
                                    </small>
                                </div>
                            </div>
                        </li>
                        <hr />
                        <li>
                            <div class="row">
                                <div class="col-md-1 col-sm-1 ml-auto mr-auto">
                                    <i class="nc-icon nc-minimal-right"></i>
                                </div>
                                <div class="col-md-11 col-sm-7  ml-auto mr-auto">
                                    <small>
                                        La fecha de inicio y finalización del primer ciclo del año 2019 aún no tiene fechas programadas, debido a esto tampoco se ha informado sobre las fechas de Formalización de la Inscripción (entrega en físico nuevamente de documentación actualizada y pagos) para los niños y niñas que realizaron la Preinscripción como antiguos según el punto 1, no está programada por no contar aún con la programación para el año 2019, esperamos que a FINALES de enero 2019 sean publicadas por este medio o en la Coordinación de la Escuela del Complejo Acuático Simón Bolívar, de igual forma si la formalización no se realiza en las fechas que se indiquen se pierde el cupo y de igual forma será habilitado para un alumno nuevo.
                                    </small>
                                </div>
                            </div>
                        </li>
                        <hr />
                        <li>
                            <div class="row">
                                <div class="col-md-1 col-sm-1 ml-auto mr-auto">
                                    <i class="nc-icon nc-minimal-right"></i>
                                </div>
                                <div class="col-md-11 col-sm-7  ml-auto mr-auto">
                                    <h6>HORARIOS ATENCIÓN ESCUELA CASB-IDRD</h6>
                                    <small>
                                        Los horarios de atención para la Escuela de Formación en Natación del CASB-IDRD para el mes de enero se realizará como se indica a continuación:
                                        - Diciembre 27, 28 y en enero se realizará entre semana en las instalaciones del IDRD, esto por cierre del Complejo Acuático desde el día 22 de diciembre hasta el 15 de enero.
                                    </small>
                                </div>
                            </div>
                        </li>
                        <hr />
                        <li>
                            <div class="row">
                                <div class="col-md-1 col-sm-1 ml-auto mr-auto">
                                    <i class="nc-icon nc-minimal-right"></i>
                                </div>
                                <div class="col-md-11 col-sm-7  ml-auto mr-auto">
                                    <small>
                                        LAS FECHAS PARA REALIZAR LAS PREINSCRIPCIONES PARA NIÑOS NUEVOS PARA LOS CICLO REGULARES POR ESTE AÑO NO ESTAN HABILITADAS debe estar atento a la información que se publique por la página de la Escuela para el año 2019 para los procesos de ingreso a la Escuela, puede consultar la información según sea el caso en esta página:
                                        <br>
                                        - En menú Bienvenidos la información de inicio y particularidades para el año 2019, sobre el proceso de inscripción en el menú           Inscripciones y las fechas que se programen para el año 2019 en el menú Cronograma y Programación.
                                    </small>
                                </div>
                            </div>
                        </li>
                        <hr />
                        <li>
                            <div class="row">
                                <div class="col-md-1 col-sm-1 ml-auto mr-auto">
                                    <i class="nc-icon nc-minimal-right"></i>
                                </div>
                                <div class="col-md-11 col-sm-7  ml-auto mr-auto">
                                    <small>
                                        Los cursos vacacionales de la Escuela del CASB ya terminaron, para el inicio del año 2019 no se tiene programados cursos vacacionales, se retomarán en la mitad del año 2019.
                                    </small>
                                </div>
                            </div>
                        </li>
                        <hr />
                        <li>
                            <div class="row">
                                <div class="col-md-1 col-sm-1 ml-auto mr-auto">
                                    <i class="nc-icon nc-minimal-right"></i>
                                </div>
                                <div class="col-md-11 col-sm-7  ml-auto mr-auto">
                                    <small>
                                        Si presenta novedades de preinscripción como nadador antiguo puede enviar un correo a la escuela del CASB – IDRD.
                                    </small>
                                </div>
                            </div>
                        </li>
                        <hr />
                    </ul>

                </div>
                <div class="modal-footer">
                    <div class="left-side">
                        <button type="button" class="btn btn-default btn-link" data-dismiss="modal">Ok</button>
                    </div>
                    <div class="divider"></div>
                    <div class="right-side">
                        <button type="button" class="btn btn-danger btn-link">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
