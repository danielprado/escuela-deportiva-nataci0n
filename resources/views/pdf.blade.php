<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
</head>

<body>
<div>

    <div style="float: center">
        <center><img style="width: 200px" src="{{ asset('public/images/Logo-rojo.png') }}"></center>  
    </div>
</div>
<div>
    <div style="text-align: center; padding-top: 130px;">
        <p style="font-family: ‘Arial Black’, Gadget, sans-serif; font-size: 16pt">ESCUELA DE NATACIÓN COMPLEJO ACUÁTICO SIMÓN BOLÍVAR -  IDRD {{\Carbon\Carbon::now()->format('Y')}}</p>
    </div>
</div>
<div>
    <div style="text-align: center;">
        <p style="font-family: ‘Arial Black’, Gadget, sans-serif; font-size: 11pt">PRE-INSCRIPCIÓN CURSOS DE NATACIÓN ALUMNOS NUEVOS</p>
    </div>
</div>

<style type="text/css">
    body {
     margin: 4rem 0;
}
 h4 {
     margin-bottom: 2rem;
     margin-top: 3rem;
}
 .panel {
     border-radius: 0.3rem;
     padding: 1rem;
     margin-bottom: 1rem;
}
 .panel.panel-blue {
     border: 1px solid #0087ff;
     background-color: #ddedff;
     color: #0087ff;
}

 .panel.panel-purple {
     border: 1px solid #C05503;
     background-color: #FFE2CC;
     color: #C05503;
}

 .panel.panel-green {
     border: 1px solid #154503;
     background-color: #BCE8AC;
     color: #154503;
}

 
</style>

<div class="container">
   <div class="row">
            <div class="col-md-6">
              <div class="panel panel-blue">INFORMACIÓN DEL NIÑO</div>

              <table cellspacing="0" style="font-size: 10pt">

                <tr style="font-family: ‘Arial Black’, Gadget, sans-serif;">
                    <td height="20px" width="320px"><strong># de inscripción - Documento de identidad</strong></td>
                    <td style="color: #0087ff; font-size: 13pt">{{ isset( $formulario->id, $formulario->cedula )? "{$formulario->id} - {$formulario->cedula}" : '' }}</td>
                   
                </tr>
            
                <tr style="font-family: ‘Arial Black’, Gadget, sans-serif;">
                    <td height="20px" width="280px"><strong>Nombre completo</strong></td>
                     <td>{{ isset( $formulario->nombre_nino, $formulario->apellido_nino ) ? "{$formulario->nombre_nino} {$formulario->apellido_nino}" : '' }}</td>
                   
                </tr>
                <tr style="font-family: ‘Arial Black’, Gadget, sans-serif;">
                    <td height="20px" width="280px"><strong>Fecha de nacicmiento y edad cumplida</strong></td>
                    <td>{{ isset( $formulario->fecha_nacimiento, $formulario->edad ) ? "{$formulario->fecha_nacimiento} - {$formulario->edad} años" : '' }}</td>
                   
                </tr>
                <tr style="font-family: ‘Arial Black’, Gadget, sans-serif;">
                    <td height="20px" width="280px"><strong>Dirección de residencia</strong></td>
                    <td>{{ isset( $formulario->direccion_nino ) ? $formulario->direccion_nino : '' }}</td>
                   
                </tr>
                <tr style="font-family: ‘Arial Black’, Gadget, sans-serif;">
                    <td height="20px" width="280px"><strong>Eps</strong></td>
                    <td>{{ isset( $formulario->eps ) ? $formulario->eps : '' }}</td>
                   
                </tr>
                <tr style="font-family: ‘Arial Black’, Gadget, sans-serif;">
                    <td height="20px" width="280px"><strong>Localidad</strong></td>
                    <td>{{ isset( $localidad->localidad ) ? $localidad->localidad : '' }}</td>
                   
                </tr>
                <tr style="font-family: ‘Arial Black’, Gadget, sans-serif;">
                    <td height="20px" width="280px"><strong>Institución educativa</strong></td>
                    <td>{{ isset( $formulario->institucion) ? "{$formulario->institucion}" : '' }}</td>
                   
                </tr>
                <tr style="font-family: ‘Arial Black’, Gadget, sans-serif;">
                    <td height="20px" width="280px"><strong>Sector del colegio</strong></td>
                    <td>{{ isset( $formulario->sector_colegio ) ? $formulario->sector_colegio : '' }}</td>
                   
                </tr>
                </tbody>
              </table>
            </div>
   <BR>

    <div class="col-md-6">
       <div class="panel panel-purple">INFORMACIÓN DEL CURSO</div>  
         <table cellspacing="0" style="font-size: 10pt">

                <tr style="font-family: ‘Arial Black’, Gadget, sans-serif;">
                    <td height="20px" width="320px"><strong>CATEGORÍA</strong></td>
                    <td style="color: #0087ff; font-size: 13pt">{{ isset( $categoria->nombre_categoria) ? "{$categoria->nombre_categoria}" : '' }}</td>
                   
                </tr>

                <tr style="font-family: ‘Arial Black’, Gadget, sans-serif;">
                    <td height="20px" width="280px"><strong>Horario del curso</strong></td>
                    <td>{{ isset( $horario->horario) ? "{$horario->horario}" : '' }}</td>
                   
                </tr>
                <tr style="font-family: ‘Arial Black’, Gadget, sans-serif;">
                    <td height="20px" width="280px"><strong>Ciclo de inscripción</strong></td>
                    <td>{{ isset( $formulario->ciclo ) ? $formulario->ciclo : '' }}</td>
                   
                </tr>
            
                </tbody>
              </table>


  <BR>

    <div class="col-md-6">
       <div class="panel panel-green">INFORMACIÓN DEL ACUDIENTE O REPRESENTANTE LEGAL</div>  
         <table cellspacing="0" style="font-size: 10pt">

                <tr style="font-family: ‘Arial Black’, Gadget, sans-serif;">
                    <td height="20px" width="320px"><strong>Nombre completo</strong></td>
                    <td>{{ isset( $formulario->nombre_acudiente ) ? $formulario->nombre_acudiente : '' }}</td>
                   
                </tr>

                <tr style="font-family: ‘Arial Black’, Gadget, sans-serif;">
                    <td height="20px" width="280px"><strong>Documento de identidad</strong></td>
                    <td>{{ isset( $formulario->cedula_acudiente ) ? $formulario->cedula_acudiente : '' }}</td>
                   
                </tr>

                <tr style="font-family: ‘Arial Black’, Gadget, sans-serif;">
                    <td height="20px" width="280px"><strong>Ocupación</strong></td>
                    <td>{{ isset( $formulario->ocupacion ) ? $formulario->ocupacion : '' }}</td>
                   
                </tr>
                <tr style="font-family: ‘Arial Black’, Gadget, sans-serif;">
                    <td height="20px" width="280px"><strong>Correo electrónico</strong></td>
                    <td>{{ isset( $formulario->mail ) ? $formulario->mail : '' }}</td>
                   
                </tr>
                <tr style="font-family: ‘Arial Black’, Gadget, sans-serif;">
                    <td height="20px" width="280px"><strong>Teléfono fijo - Celular</strong></td>
                   <td>{{ isset( $formulario->telefono, $formulario->celular )? "{$formulario->telefono} - {$formulario->celular}" : '' }}</td>
                   
                </tr>
                
            
                </tbody>
              </table>



    </div>
  </div>
</div>


<div>
   
</div>
</body>
</html>