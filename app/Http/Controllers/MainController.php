<?php 

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Idrd\Usuarios\Repo\PersonaInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Form;
use App\Localidad;
use App\Upz;
use App\Barrio;
use App\Poblacion;
use App\Deporte;
use App\Horario;
use App\Categoria;
use App\Escenario;
use App\Cupo;
use Mail;

class MainController extends Controller {

	protected $Usuario;
	protected $repositorio_personas;

	public function __construct(PersonaInterface $repositorio_personas)
	{
		if (isset($_SESSION['Usuario']))
			$this->Usuario = $_SESSION['Usuario'];

		$this->repositorio_personas = $repositorio_personas;
	}

	public function welcome()


	{

	 return view('welcome');

	}

    public function registro()


  {

   $localidad = Localidad::all();
   $upz = Upz::all();
   $barrio = Barrio::all();
   $poblacion = Poblacion::all();
   $horario = Horario::all();
   $categoria = Categoria::all();
   return view('registro',["localidades"=>$localidad, "poblaciones"=>$poblacion,"horarios"=>$horario, "categorias"=>$categoria]);

  }

    public function index(Request $request)
	{
		$fake_permissions = ['5144', '1'];
		//$fake_permissions = null;

		if ($request->has('vector_modulo') || $fake_permissions)
		{	
			$vector = $request->has('vector_modulo') ? urldecode($request->input('vector_modulo')) : $fake_permissions;
			$user_array = is_array($vector) ? $vector : unserialize($vector);
			$permissions_array = $user_array;

			$permisos = [
				'permiso1' => array_key_exists(1, $permissions_array) ? intval($permissions_array[1]) : 0
			];

			$_SESSION['Usuario'] = $user_array;
            $persona = $this->repositorio_personas->obtener($_SESSION['Usuario'][0]);

			$_SESSION['Usuario']['Recreopersona'] = [];
			$_SESSION['Usuario']['Roles'] = [];
			$_SESSION['Usuario']['Persona'] = $persona;
			$_SESSION['Usuario']['Permisos'] = $permisos;
			$this->Usuario = $_SESSION['Usuario'];
		} else {
			if (!isset($_SESSION['Usuario']))
				$_SESSION['Usuario'] = '';
		}

		if ($_SESSION['Usuario'] == '')
			return redirect()->away('http://www.idrd.gov.co/SIM/Presentacion/');

		return redirect('/welcome');
	}

	public function logout()
	{
		$_SESSION['Usuario'] = '';
		Session::set('Usuario', ''); 

		return redirect()->to('/');
	}

   private function cifrar($M)

    {   

      $C="";

      $k = 18; 

      for($i=0; $i<strlen($M); $i++)$C.=chr((ord($M[$i])+$k)%255);

      return $C;

    }



    private function decifrar($C)

    {   

      $M="";

      $k = 18;

      for($i=0; $i<strlen($C); $i++)$M.=chr((ord($C[$i])-$k+255)%255);

      return $M;

    }



     public function listar_datos()

    {



      //$acceso = Form::where('fecha_caminata','2019-03-17')->whereYear('created_at', '=', date('Y'))->get();

    $acceso = Form::with('deportes')->whereYear('created_at', '=', date('Y'))->get(); 



      $tabla='<table id="fresh-table" class="table" data-show-export="true" data-locale="es-MX">



        <thead>

           <tr>

             <th style="text-transform: capitalize;">id</th>

             <th style="text-transform: capitalize;">NOMBRES COMPLETOS</th>

             <th style="text-transform: capitalize;">APELLIDOS COMPLETOS</th>

             <th style="text-transform: capitalize;">TIPO DE DOCUMENTO</th>

             <th style="text-transform: capitalize;">DOCUMENTO</th>

             <th style="text-transform: capitalize;">EPS</th>

             <th style="text-transform: capitalize;">IDENTIDAD DE GÉNERO</th>

             <th style="text-transform: capitalize;">SEXO</th>

             <th style="text-transform: capitalize;">CORREO ELECTRÓNICO</th>

             <th style="text-transform: capitalize;">NOMBRE DE CONTACTO</th>

             <th style="text-transform: capitalize;">INSTITUTO</th>

             <th style="text-transform: capitalize;"> DIRECCIÓN</th>   

             <th style="text-transform: capitalize;">CÓDIGO POSTAL</th>  

             <th style="text-transform: capitalize;"> CIUDAD</th>  

             <th style="text-transform: capitalize;">PAÍS</th>  

             <th style="text-transform: capitalize;">EMAIL</th>      

          </tr>

        </thead>

        <tbody id="tabla">';



      foreach ($acceso as $key => $value)

      {



       $tabla.='<tr><td>'.$value->id.'</td>';

       $tabla.='<td>'.$value->producto.'</td>';

       $tabla.='<td>'.$value->producto_dos.'</td>';

       $tabla.='<td>'.Linea::find($value->linea_investigacion)->Nombre_Linea.'</td>';

       $tabla.='<td>'.$value->palabra_clave_1.' - '.$value->palabra_clave_2.' - '.$value->palabra_clave_3.'</td>';

       $tabla.='<td>'.$value->titulo_resumen.'</td>';

       $tabla.='<td>'.$value->correo_autor.'</td>';

       $tabla.='<td>'.$value->titulo.' - '.$value->nombre_autor.' - '.$value->apellido_autor.'</td>';

       $tabla.='<td>'.$value->resumen.'</td>';

       $tabla.='<td>'.$value->titulo_contacto.' - '.$value->nombre_contacto.' - '.$value->apellido_contacto.'</td>';

       $tabla.='<td>'.$value->instituto.'</td>';

       $tabla.='<td>'.$value->direccion.'</td>';

       $tabla.='<td>'.$value->codigo_postal.'</td>';

       $tabla.='<td>'.$value->ciudad_contacto.'</td>';

       $tabla.='<td>'.Pais::find($value->pais_contacto)->Nombre_Pais.'</td>';

       $tabla.='<td>'.$value->correo_contacto.'</td></tr>';

      }



      $tabla.='</tbody></table>';

      return view('tabla', ['tabla' => $tabla]);

    }



public function logear(Request $request)

    {



      $usuario = $request->input('usuario');

      $pass = $request->input('pass');

      $acceso = Acceso::where('Usuario',$usuario)->where('Contrasena', sha1($this->cifrar($pass)) )->first();



      if (empty($usuario)) { return view('error',['error' => 'Usuario o contraseña invalida!'] ); exit(); }

      if (empty($acceso)) { return view('error',['error' => 'Usuario o contraseña invalida!'] ); exit(); }

       

      // session_start() ;

       $_SESSION['id_usuario'] = json_encode($acceso);



      return redirect()->route('index.lista.datos');

    }



//función para validar si la cédula ya fue censada

   public function validar_cedula(Request $request)
   {
     if ($request-> has('cedula')) {
          $usuario = Form::where('cedula', $request->input('cedula'))->first(); 
     if (!empty($usuario)) { return response()-> json(['error'=>'Usted ya está censado  ']); exit(); }
     }

   }

//función para insertar 
public function insertar(Request $request)

    {

     $post = $request->input();
     $usuario = Form::where('cedula', $request->input('cedula'))->first(); 
     if (!empty($usuario)) { return view('error',['error' => 'Este usuario ya fue registrado!'] ); exit(); 
    }
     $formulario = new Form([]);

      //envio de correo

     if($this->inscritos($request))

     {

        $formulario = $this->store($formulario, $request);

     // $this->store($formulario, $request);

            $categoria = Categoria::findOrFail( $formulario->categoria );
            $localidad = Localidad::findOrFail( $formulario->localidad );
            $horario = Horario::findOrFail( $formulario->horario );
        
        Mail::send('email', ['user' => $request->input('mail'),'formulario' => $formulario, 'categoria' => $categoria, 'localidad' =>$localidad, 'horario' =>$horario], function ($m) use ($request) 
        {
            $m->from('no-reply@idrd.gov.co', 'Pre-inscripción aceptada al curso de Natación ciclo 8-2019 ');
            $m->to($request->input('mail'), $request->input('primer_nombre'))->subject('Pre-inscripción aceptada al curso de Natación ciclo 8-2019');
        });

      }else{
        return view('error', ['error' => 'Lo sentimos ya NO hay cupos en este horario!']);
      }
          return view('error', ['error' => 'SU PRE-INSCRIPCIÓN FUE ACEPTADA Recuerde Imprimir el comprobante de Pre-Inscripción (Descargar Formato de Pre-Inscripción) y Formalizar la Inscripción (entrega de documentación) del niño o niña en el escenario donde se inscribió entre el 07 y el 12 de noviembre de 2019', 'download' => $formulario]);
    }


 //funcion validad edad con la categoría

    public function validar(Request $request)
    {
      $edad = $request->input('edad');

      $categoria = Categoria::where('min', '<=', $edad)
                            ->where('max',  '>=', $edad)
                            ->first();

      return response()->json(['categoria' => $categoria]);
    }

// validar categoria - escenario
    public function validar_escenario(Request $request)
    {
    //modelo de la relación
      $categoria = Categoria::with('escenarios')->find($request->categoria);

      $select = ' <option value="">Seleccione</option>';
      foreach ($categoria->escenarios as $escenario) {

        $select .='<option value="'.$escenario->id_esc.'">'.$escenario->nom_esc.'</option>';        
      }
        
        return $select;

    }

    //validar escenario - horario
    public function validar_horario(Request $request)
    {
    //horarios es el modelo de la relación
      $escenarios = Escenario::with('horarios')->find($request->escenarios);

      $select = ' <option value="">Seleccione</option>';
      foreach ($escenarios->horarios as $horario) {

        $select .='<option value="'.$horario->id.'">'.$horario->horario.'</option>';        
      }
        
        return $select;

    }

//validar localidad

    public function validar_localidad(Request $request)
    {
    
      $upz = Upz::where('IdLocalidad',$request->localidad)->get();

      $select = ' <option value="">Seleccione</option>';
      foreach ($upz as $upecetas) {

        $select .='<option value="'.$upecetas->cod_upz.'">'.$upecetas->Upz.'</option>';        
      }
        
        return $select;

    }

    //validar upz

    public function validar_upz(Request $request)
    {
    
      $barrios = Barrio::where('CodUpz',$request->upz)->get();

      $select = ' <option value="">Seleccione</option>';
      foreach ($barrios as $barrio) {

        $select .='<option value="'.$barrio->IdBarrio.'">'.$barrio->Barrio.'</option>';        
      }
        
        return $select;

    }

// conteo de la tabla

    private function inscritos($request)
    {

    
      $cupo = Cupo::where(['id_horario' => $request['horario'],'id_categoria'=> $request['categoria'] ])->first()['cupo'];
      $cant = Form::where(['horario' => $request['horario'],'categoria'=> $request['categoria'] ])->count('id');


      if($cant>=$cupo){return false;}
      if($cant<$cupo){return true;}

    }
//función contador de cupos
   public function contador(Request $request)
   {
     $cupo = Cupo::where(['id_horario' => $request['horario'],'id_categoria'=> $request['categoria'] ])->first()['cupo'];
     $cant = Form::where(['horario' => $request['horario'],'categoria'=> $request['categoria'] ])->count('id');
    return response()->json(['contador'=>$cupo, 'inscritos'=>$cant, 'restantes'=>$cupo-$cant]);
   }
    

   private function store($formulario, $input)
//input lo que viene del form
// formulario es la variable de la base de datos
    {

      /* datos del niño*/
        $formulario['nombre_nino'] = $input['nombre_nino'];
        $formulario['apellido_nino'] = $input['apellido_nino'];
        $formulario['cedula'] = $input['cedula'];
        $formulario['fecha_nacimiento'] = $input['fecha_nacimiento'];
        $formulario['edad'] = $input['edad'];
        $formulario['sexo'] = $input['sexo'];
        $formulario['genero'] = $input['genero'];
        $formulario['etnia'] = $input['etnia'];
        $formulario['poblacion'] = $input['poblacion'];
        $formulario['discapacidad'] = $input->input('discapacidad', '');
        $formulario['direccion_nino'] = $input['direccion_nino'];
        $formulario['eps'] = $input['eps'];
        $formulario['localidad'] = $input['localidad'];
        $formulario['upz'] = $input['upz'];
        $formulario['barrio'] = $input['barrio'];
        $formulario['estrato'] = $input['estrato'];
        $formulario['institucion'] = $input['institucion'];
        $formulario['sector_colegio'] = $input['sector_colegio'];
        /*datos del cursos*/
        $formulario['categoria'] = $input['categoria'];
        $formulario['escenario'] = $input['escenario'];
        $formulario['horario'] = $input['horario'];
        /* datos del acudiente*/
        $formulario['nombre_acudiente'] = $input['nombre_acudiente'];
        $formulario['cedula_acudiente'] = $input['cedula_acudiente'];
        $formulario['ocupacion'] = $input['ocupacion'];
        $formulario['mail'] = $input['mail'];
        $formulario['telefono'] = $input['telefono'];
        $formulario['celular'] = $input['celular'];
        $formulario['ciclo'] = $input['ciclo'];
           
        $formulario->save();

        return $formulario;
    } 
}