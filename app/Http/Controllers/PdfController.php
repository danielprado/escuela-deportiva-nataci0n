<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB as DB;
use Validator;
use Session;
use App\Form;
use App\Localidad;
use App\Categoria;
use App\Horario;

class PdfController extends Controller
{
    public function pdf($id)
    {

        if ( isset( $id ) ) {
            $formulario = Form::findOrFail( $id );
            $categoria = Categoria::findOrFail( $formulario->categoria );
            $localidad = Localidad::findOrFail( $formulario->localidad );
            $horario = Horario::findOrFail( $formulario->horario );
            $view = view('pdf', ['formulario' => $formulario, 'categoria' => $categoria, 'localidad' =>$localidad, 'horario' =>$horario])->render();
            $pdf = \PDF::loadHTML($view);
            return $pdf->setPaper('a4', 'portrait')->stream('Comprobante');
        } else {
            return view('error', ['error' => 'No se encuentran registros para este usuario.']);
        }

    }
    public function document(Request $request)
    {

        if ( $request->has( 'cedula' ) ) {
            $formulario = Form::where('cedula', $request->get('cedula'))->where('id',$request->get('respuestas_fc'))->first();
            if (!$formulario) {
                 return view('error', ['error' => 'No se encuentran registros para este usuario.']);
    
            }
            $categoria = Categoria::findOrFail( $formulario->categoria );
            $localidad = Localidad::findOrFail( $formulario->localidad );
            $horario = Horario::findOrFail( $formulario->horario );
            $view = view('pdf', ['formulario' => $formulario, 'categoria' => $categoria, 'localidad' =>$localidad, 'horario' =>$horario])->render();
            $pdf = \PDF::loadHTML($view);
            return $pdf->setPaper('a4', 'portrait')->stream('Comprobante');
        } else {
            return view('error', ['error' => 'No se encuentran registros para este usuario.']);
        }

    }

    public function preguntas (Request $request)
    {

        if ($request->isMethod('post') and $request->has('cedula')) {
            $usuario = Form::where('cedula', $request->input('cedula'))->first(); 

            if (!isset($usuario->id)) {
                return view('error', ['error' => 'No se encuentran registros para este usuario.']);
            }

            $incorrectas=Form::select(['fecha_nacimiento','id'])->inRandomOrder()->take( 4 )->get();
            $respuestas=[];
            foreach ($incorrectas as $value) {
               $respuestas[]=['id' => isset( $value->id ) ? $value->id : null,'fecha_nacimiento' => isset( $value->fecha_nacimiento ) ? $value->fecha_nacimiento : null];
            }

            $correctas=['id' => isset( $usuario->id ) ? $usuario->id : null,'fecha_nacimiento' => isset( $usuario->fecha_nacimiento ) ? $usuario->fecha_nacimiento : null];

            $daniel=array_prepend($respuestas,$correctas);

            shuffle($daniel);
            return view('descarga',['aleatorio'=>$daniel,'cedula'=>$request->input('cedula')]);

            
        }else
        {
            return view('descarga');
        }
    }

}
