<?php
session_start();
/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/personas', '\Idrd\Usuarios\Controllers\PersonaController@index');
Route::get('/personas/service/obtener/{id}', '\Idrd\Usuarios\Controllers\PersonaController@obtener');
Route::get('/personas/service/buscar/{key}', '\Idrd\Usuarios\Controllers\PersonaController@buscar');
Route::get('/personas/service/ciudad/{id_pais}', '\Idrd\Usuarios\Controllers\LocalizacionController@buscarCiudades');
Route::post('/personas/service/procesar/', '\Idrd\Usuarios\Controllers\PersonaController@procesar');

Route::any('/', 'MainController@index');
Route::any('/logout', 'MainController@logout');

//rutas con filtro de autenticación
Route::group(['middleware' => ['web']], function () {

    Route::get('/welcome', 'MainController@welcome');

    Route::get('/registro', 'MainController@registro');

	Route::get('/recomendaciones', function(){
		return view('recomendaciones');
	});

	/*Route::get('/descarga', function(){
		return view('descarga');
	});*/

	Route::get('/reglamento', function () {
	    return view('reglamento');
    });

    Route::get('/pago', function () {
	    return view('pago');
    });

   Route::get('/consultas', function () {
	    return view('consultas');
    });

    Route::get('/inscripciones', function () {
	    return view('inscripciones');
    });

    Route::get('/comunicados', function () {
	    return view('comunicados');
    });

    Route::get('/clavados', function () {
	    return view('clavados');
    });

    Route::get('login', function () {                
    return view('login');
    });
  /*Route::get('/registro', function () {
	    return view('registro');
    });*/
    Route::get('pdf/{id}', 'PdfController@pdf')->name('to_print');
    Route::post('comprobante', 'PdfController@document');
    Route::match(['get','post'],'descarga', 'PdfController@preguntas');

     Route::post('download', 'PdfController@document');

    Route::get('/cronograma', function () {
	    return view('cronograma');
    });

   
		Route::any('logear','MainController@logear');
		Route::any('ultimo_codigo','MainController@last_codigo');
		Route::any('carnet','PdfController@carnet');
		Route::post('insertar', 'MainController@insertar');
		Route::post('validar_localidad', 'MainController@validar_localidad');
		Route::post('validar_upz', 'MainController@validar_upz');
		Route::post('validar_cedula', 'MainController@validar_cedula');
		Route::post('validar', 'MainController@validar');
		Route::post('validar_horario', 'MainController@validar_horario');
		Route::post('validar_escenario', 'MainController@validar_escenario');
		Route::post('contador', 'MainController@contador');
		Route::post('eliminar', 'MainController@eliminar');
		Route::any('listar_pais', 'MainController@listar_pais');
		Route::any('listar_ciudad', 'MainController@listar_ciudad');
		Route::any('listar_localidad', 'MainController@listar_localidad');
		Route::any('listar_departamento', 'MainController@listar_departamento');
		Route::any('listar_datos', 'MainController@listar_datos');
		Route::any('listar_datos1', 'MainController@listar_datos1');
		Route::get('/personas/service/ciudad/{id_pais}', '\Idrd\Usuarios\Controllers\LocalizacionController@buscarCiudades');
		Route::post('/personas/service/procesar/', '\Idrd\Usuarios\Controllers\PersonaController@procesar');

		Route::group(['middleware' => ['web']], function () {

		    //

		});


});
/*