<?php


namespace App;
use Illuminate\Database\Eloquent\Model;

class Escenario  extends Model

{

  protected $table = 'escenarios';

  protected $primaryKey = 'id_esc';

  protected $fillable = ['nom_esc'];

  public function horarios(){
	
	return $this->belongsToMany('App\Horario','cupos_CABS_2019','id_escenario','id_horario')->withPivot('id_categoria');

   }
    

}
