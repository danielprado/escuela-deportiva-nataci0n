<?php

namespace App;

use Idrd\Parques\Repo\Upz as MUpz;
use Illuminate\Database\Eloquent\Model;

class Upz extends Model
{
	protected $connection='db_parques';
    protected $table = 'upz';
    protected $primaryKey = 'cod_upz';
    protected $fillable = ['Upz', 'cod_upz'];
    public $timestamps = false;


    public function localidad()
    {
        return $this->belongsTo(Localidad::class, 'IdLocalidad');
    }

    public function parques()
    {
        return $this->hasMany(config('parques.modelo_parque'), 'Upz');
    }

    public function barrios()
    {
        return $this->hasMany(Barrio::class, 'CodUpz', 'cod_upz');
    }
}