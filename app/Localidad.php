<?php

namespace App;

use Idrd\Parques\Repo\Localidad as MLocalidad;
use Illuminate\Database\Eloquent\Model;

class Localidad extends Model

{
	protected $connection='db_parques';
    protected $table = 'localidad';
    protected $primaryKey = 'Id_Localidad';
    protected $fillable = ['Localidad'];
    protected $appends = ['Localidad'];
    public $timestamps = false;

    public function getLocalidadAttribute( $value )
    {
        if ( isset( $value ) ) {
            return $value;
        }

        if ( isset( $this->Nombre_Localidad ) ) {
            return $this->Nombre_Localidad;
        }
    }


    public function parques()
    {
        return $this->hasMany(config('parques.modelo_parque'), 'Id_Localidad');
    }

    public function upz()
    {
        return $this->hasMany(Upz::class, 'IdLocalidad');
    }
}