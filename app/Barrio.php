<?php

namespace App;

use Idrd\Parques\Repo\Barrio as MBarrio;
use Illuminate\Database\Eloquent\Model;

class Barrio extends Model
{
	protected $connection='db_parques';
    protected $table = 'Barrios';
    protected $primaryKey = 'IdBarrio';
    protected $fillable = ['Barrio', 'CodUpz'];
    public $timestamps = false;

    public function upz()
    {
        return $this->belongsTo(Upz::class, 'CodUpz', 'cod_upz');
    }
}