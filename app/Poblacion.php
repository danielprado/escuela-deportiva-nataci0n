<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Poblacion extends Model

{

  protected $table = 'grupo_poblacional';

  protected $primaryKey = 'id';

  protected $fillable = ['nombre'];
}
