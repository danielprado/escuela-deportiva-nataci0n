<?php


namespace App;
use Illuminate\Database\Eloquent\Model;

class Categoria extends Model

{

  protected $table = 'categoria_CASB_2019';

  protected $primaryKey = 'id';

  protected $fillable = ['nombre_categoria'];

   public function escenarios(){
	
	return $this->belongsToMany('App\Escenario','cupos_CABS_2019','id_categoria','id_escenario')->withPivot('id_horario');

   }


 /* public function horarios(){
	
	return $this->belongsToMany('App\Horario','cupos_CABS_2019','id_categoria','id_horario');

   }*/
    

}
