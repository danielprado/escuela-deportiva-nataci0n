<?php


namespace App;
use Illuminate\Database\Eloquent\Model;

class Horario extends Model

{

  protected $table = 'horarios_CASB_2019';

  protected $primaryKey = 'id';

  protected $fillable = ['horario'];

   public function categorias(){
	
	return $this->belongsToMany('App\Categoria','cupos_CABS_2019','id_horario','id_categoria')->withPivot('id_escenario');

   }
}
